package grand.app.alamghareeb.network

import grand.app.alamghareeb.dialogs.specialorder.model.SpecialOrderRequest
import grand.app.alamghareeb.main.editprofile.request.EditProfileRequest
import grand.app.alamghareeb.utils.*
import java.io.File

class ApiRepo(private val apiInterface: ApiInterface) {

    suspend fun requestPostBody(url: String?, requestData: Any?) = apiInterface.post(url, requestData)

    suspend fun requestPostMap(url: String?, requestData: Any?) = apiInterface.post(url, getParameters(requestData))

    suspend fun requestGet(url: String?) = apiInterface.get(url)

    suspend fun requestGetQuery(url: String?, id: Int) = apiInterface.getQuery(url, id)

    suspend fun filterOrders(url: String?, orderBy: String) = apiInterface.filterOrders(url, orderBy)

    suspend fun filterProducts(url: String?, orderBy: String) = apiInterface.filterProducts(url, orderBy)

    suspend fun applyCoupon(url: String?, orderBy: String?) = apiInterface.applyCoupon(url, orderBy)

    suspend fun getProductsByCategoryId(url: String?, id: Int) = apiInterface.getProductsByCategory(url, id)

    suspend fun getFilterDetails(url: String?, id: Int) = apiInterface.getFilterDetails(url, id)

    suspend fun getProductsBySubCategoryId(url: String?, catId: Int, subCatId: Int) = apiInterface.getProductsBySubCategory(url, catId, subCatId)

    suspend fun deleteAddress(url: String?, id: Int) = apiInterface.deleteQuery(url, id)

    suspend fun updateProfile(request : EditProfileRequest) = apiInterface.updateProfile(
        request.name?.toRequestBodyParam(),
        request.email?.toRequestBodyParam(),
        request.phone?.toRequestBodyParam(),
        request.userImg?.toMultiPart("image")
    )

    suspend fun requestDeleteQuery(url: String?, id: Int) = apiInterface.deleteQuery(url, id)

    suspend fun requestMultiPart(url: String?, requestData: Any?, img: File?, partName: String) =
        apiInterface.requestPostMultiPart(url,
            getParameters(requestData),
            img?.toMultiPart(partName))

    suspend fun specialOrder(request: SpecialOrderRequest) =
        apiInterface.specialOrder(
            request.name?.toRequestBodyParam(),
            request.price?.toRequestBodyParam(),
            request.quantity.toRequestBodyParam(),
            request.description?.toRequestBodyParam(),
            request.product_image?.toMultiPart("image")
        )
}

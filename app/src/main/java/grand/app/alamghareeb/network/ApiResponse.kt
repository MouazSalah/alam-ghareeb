package grand.app.alamghareeb.network

/*
 * Created by Mouaz Salah on 23/06/2021.
 */
data class ApiResponse<out T>(val status: Status, val data: T? = null, val message: String? = null) {

    companion object {
        fun <T> success(data: T?, msg: String? = null): ApiResponse<T> {
            return ApiResponse(Status.SUCCESS, data, msg)
        }
        fun <T> successMessage(msg: String?, data: T? = null): ApiResponse<T> {
            return ApiResponse(Status.SUCCESS_MESSAGE, data, msg)
        }
        fun <T> errorMessage(msg: String?, data: T? = null): ApiResponse<T> {
            return ApiResponse(Status.ERROR_MESSAGE, data, msg)
        }
        fun <T> shimmerLoading(data: T?): ApiResponse<T> {
            return ApiResponse(Status.SHIMMER_LOADING, data)
        }
        fun <T> progressLoading(Boolean: T?): ApiResponse<T> {
            return ApiResponse(Status.PROGRESS_LOADING, Boolean)
        }
        fun <T> notLogin(data: T?): ApiResponse<T> {
            return ApiResponse(Status.NOT_LOGIN, data)
        }
    }
}
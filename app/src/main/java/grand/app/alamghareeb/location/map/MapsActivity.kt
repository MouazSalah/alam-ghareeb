package grand.app.alamghareeb.location.map

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import grand.app.alamghareeb.activity.BaseActivity
import grand.app.alamghareeb.databinding.ActivityMapsBinding
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import timber.log.Timber
import grand.app.alamghareeb.R

class MapsActivity : BaseActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private var hasIntent: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)

        when {
            intent.hasExtra(Params.BRANCH_NAME) -> {
                when {
                    intent.getStringExtra(Params.BRANCH_NAME) != null -> {
                        hasIntent = true
                        binding.addressLayout.visibility = View.VISIBLE
                        binding.tvSalonName.text = intent.getStringExtra(Params.BRANCH_NAME)
                        Timber.e("mou3az_map : " + intent.getStringExtra(Params.BRANCH_NAME))
                    }
                }
            }
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment =
            supportFragmentManager.findFragmentById(grand.app.alamghareeb.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val myLocation = LatLng(intent.getDoubleExtra(Params.BRANCH_LAT, 30.043917),
            intent.getDoubleExtra(Params.BRANCH_LONG, 31.010492))
        mMap.addMarker(MarkerOptions().position(myLocation))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, Const.zoomLevel))


        Timber.e("mou3az_map : " + intent.getDoubleExtra(Params.BRANCH_LAT, 0.0))
        Timber.e("mou3az_map : " + intent.getDoubleExtra(Params.BRANCH_LONG, 0.0))

        mMap.setOnMapClickListener { point: LatLng ->
            googleMap.clear()
            val userLocation = LatLng(point.latitude, point.longitude)
            val option =
                MarkerOptions().position(LatLng(userLocation.latitude, userLocation.longitude))
            mMap.addMarker(option)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(userLocation.latitude,
                userLocation.longitude), Const.zoomLevel))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
        setResult(Codes.GET_LOCATION_FROM_MAP, intent)
        finish()
    }
}
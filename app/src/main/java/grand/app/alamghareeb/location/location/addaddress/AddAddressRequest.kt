package grand.app.alamghareeb.location.location.addaddress

data class AddAddressRequest(
    var name: String? = null,
    var phone: String? = null,
    var city_id: Int? = null,
    var area_id: Int? = null,
    var street: String? = null,
    var building_no: String? = null,
    var floor: String? = null,
    var special_marque: String? = null,
    var additional_phone: String? = null
)
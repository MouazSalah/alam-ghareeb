package grand.app.alamghareeb.auth.resetpass.model

data class ResetRequest (
        var password: String? = null,
        var password_confirmation: String? = null
)
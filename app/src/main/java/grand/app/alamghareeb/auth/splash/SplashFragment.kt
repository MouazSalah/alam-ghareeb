package grand.app.alamghareeb.auth.splash

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.BaseAuthFragment
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.databinding.FragmentSplashBinding
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Const
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.core.registry.PropertyRegistry
import timber.log.Timber

class SplashFragment : BaseAuthFragment()
{
    lateinit var binding: FragmentSplashBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        when (Const.isAskedToLogin) {
            1 -> {
                findNavController().navigate(R.id.splash_to_login)
            }
            else -> {
                startTime()
            }
        }
    }

    private fun startTime()
    {
        lifecycleScope.launch {
            delay(2500)
            when {
                PrefMethods.getUserData() == null -> {
                    findNavController().navigate(R.id.splash_to_login)
                }
                else -> {
                    requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java))
                    requireActivity().finishAffinity()
                }
            }
        }
    }
}
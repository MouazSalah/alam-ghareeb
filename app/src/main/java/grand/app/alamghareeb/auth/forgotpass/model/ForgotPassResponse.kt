package grand.app.alamghareeb.auth.forgotpass.model

import com.google.gson.annotations.SerializedName

data class ForgotPassResponse(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("code")
	val code: Int? = null
)

package grand.app.alamghareeb.auth.verifycode.view

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.BaseAuthFragment
import grand.app.alamghareeb.auth.verifycode.model.VerifyResponse
import grand.app.alamghareeb.auth.verifycode.viewmodel.VerifyViewModel
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentVerifyCodeBinding
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject


class VerifyFragment : BaseAuthFragment() {
    lateinit var binding: FragmentVerifyCodeBinding
    @Inject
    lateinit var viewModel: VerifyViewModel
    private val fragmentArgs: VerifyFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_verify_code, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        viewModel.request.phone = fragmentArgs.phone
        when (fragmentArgs.flag) {
            Codes.REGISTER_INTENT -> {
                binding.tvVerifyTitle.text = getString(R.string.label_activate_title)
                binding.tvVerifyDesc.text = getString(R.string.label_activate_description)
                viewModel.request.type = "verify"
            }
            Codes.FORGOT_INTENT -> {
                binding.tvVerifyTitle.text = getString(R.string.label_verify_title)
                binding.tvVerifyDesc.text = getString(R.string.label_verify_description)
                viewModel.request.type = "reset"
            }
        }



        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is VerifyResponse -> {
                            when (fragmentArgs.flag) {
                                Codes.REGISTER_INTENT -> {
                                    when {
                                        Const.isAskedToLogin == 1 -> {
                                            requireActivity().finish()
                                        }
                                        else -> {
                                            findNavController().navigate(R.id.verify_to_login)
                                        }
                                    }
                                }
                                else -> {
                                    findNavController().navigate(VerifyFragmentDirections.verifyToReset(
                                        viewModel.request.phone!!,
                                        it.data.data!!.token.toString()))
                                }
                            }
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.REGISTER_INTENT -> {
                                    findNavController().navigate(R.id.login_to_register)
                                }
                                Codes.FORGOT_INTENT -> {
                                    findNavController().navigate(R.id.login_to_forgot_password)
                                }
                                Codes.SKIP_CLICKED -> {
                                    findNavController().navigate(R.id.login_to_home)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}
package grand.app.alamghareeb.auth.verifycode.model

import com.google.gson.annotations.SerializedName

data class VerifyResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class Data(

	@field:SerializedName("token")
	val token: String? = null
)

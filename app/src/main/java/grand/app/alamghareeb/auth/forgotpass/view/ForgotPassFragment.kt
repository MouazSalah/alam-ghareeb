package grand.app.alamghareeb.auth.forgotpass.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.BaseAuthFragment
import grand.app.alamghareeb.auth.forgotpass.model.ForgotPassResponse
import grand.app.alamghareeb.auth.forgotpass.viewmodel.ForgotPassViewModel
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentForgotPasswordBinding
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class ForgotPassFragment : BaseAuthFragment()
{
    lateinit var binding: FragmentForgotPasswordBinding
    @Inject lateinit var viewModel: ForgotPassViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ForgotPassResponse -> { viewModel.request.phone?.let { it1 ->
                                ForgotPassFragmentDirections.forgotToVerify(it1, Codes.FORGOT_INTENT)
                            }?.let {
                                    it2 -> findNavController().navigate(it2) }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}
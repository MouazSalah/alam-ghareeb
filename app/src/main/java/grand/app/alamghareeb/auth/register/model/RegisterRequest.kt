package grand.app.alamghareeb.auth.register.model

data class RegisterRequest (
        var name: String? = null,
        var email: String? = null,
        var phone: String? = null,
        var password: String? = null,
        var device_token: String? = null)
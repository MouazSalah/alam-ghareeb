package grand.app.alamghareeb.auth.conditions

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentAboutAppBinding
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.main.aboutapp.viewmodel.AboutViewModel
import grand.app.alamghareeb.main.addaddress.model.AddAddressResponse
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class ConditionsActivity : AppCompatActivity()
{
    lateinit var binding: FragmentAboutAppBinding
    @Inject lateinit var viewModel: AboutViewModel

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_about_app)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        viewModel.getDetails(URLS.TERMS_AND_CONDITIONS)
        binding.tvTitle.text = getString(R.string.terms_conditions)

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddAddressResponse -> {
                            val intent = Intent()
                            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                            setResult(Codes.ACCEPT_CONDITIONS, intent)
                            finish()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        binding.ibBack.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            setResult(Codes.ACCEPT_CONDITIONS, intent)
            finish()
        }
    }

    private fun showToast(msg : String, type : Int) {
        // success 2
        // false  1
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(this, DialogToastFragment::class.java.name, Codes.DIALOG_TOAST_REQUEST, bundle)
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
        setResult(Codes.ACCEPT_CONDITIONS, intent)
        finish()
    }
}


package grand.app.alamghareeb.auth.register.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.google.firebase.messaging.FirebaseMessaging
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.BaseAuthFragment
import grand.app.alamghareeb.auth.conditions.ConditionsActivity
import grand.app.alamghareeb.auth.register.model.RegisterResponse
import grand.app.alamghareeb.auth.register.viewmodel.RegisterViewModel
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentRegisterBinding
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class RegisterFragment : BaseAuthFragment()
{
    lateinit var binding : FragmentRegisterBinding
    @Inject lateinit var viewModel : RegisterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            when {
                !task.isSuccessful -> {
                    Timber.e(task.exception.toString())
                    return@addOnCompleteListener
                }
                task.result != null -> {
                    viewModel.request.device_token = task.result
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is RegisterResponse -> {
                            val action = RegisterFragmentDirections.registerToVerify(viewModel.request.phone!!, Codes.REGISTER_INTENT)
                            findNavController().navigate(action)
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.TERMS_CLICKED -> {
                                    requireActivity().startActivityForResult(Intent(requireActivity(), ConditionsActivity::class.java), Codes.ACCEPT_CONDITIONS)
                                }
                                Codes.BACK_PRESSED -> {
                                    findNavController().navigateUp()
                                }
                                Codes.LOGIN_INTENT -> {
                                    findNavController().navigate(R.id.register_to_login)
                                }
                                Codes.SKIP_CLICKED -> {
                                    findNavController().navigate(R.id.register_to_login)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.ACCEPT_CONDITIONS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        binding.cbTerms.isChecked = true
                                        viewModel.obsIsTermsAccepted.set(true)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
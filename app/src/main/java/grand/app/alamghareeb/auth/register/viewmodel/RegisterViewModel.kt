package grand.app.alamghareeb.auth.register.viewmodel

import androidx.databinding.ObservableBoolean
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.auth.register.model.RegisterRequest
import grand.app.alamghareeb.auth.register.model.RegisterResponse
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall
import timber.log.Timber
import javax.inject.Inject

open class RegisterViewModel @Inject constructor() : BaseViewModel()
{
    var request = RegisterRequest()
    var obsIsTermsAccepted = ObservableBoolean()

    fun onRegisterClicked()
    {
        setClickable()
        when {
            request.name.isNullOrEmpty() || request.name.isNullOrBlank() -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_name))
            }
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_invalid_phone))
            }
            request.email.isNullOrEmpty() || request.email.isNullOrBlank() -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_email))
            }
            request.password.isNullOrEmpty() || request.password.isNullOrBlank() -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_password))
            }
            request.password!!.length < 6 -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_invalid_password))
            }
            !obsIsTermsAccepted.get() -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_terms_should_be_accepted))
            }
            else -> {
                register()
            }
        }
    }

    fun register() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.REGISTER, request)}) { res ->
            obsIsProgress.set(false)
            val response : RegisterResponse = Gson().fromJson(res, RegisterResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onButtonClicked(value : Int) {
        Timber.e("terms clicked")
        setClickable()
        apiResponseLiveData.value = ApiResponse.success(value)
    }
}
package grand.app.alamghareeb.utils.resources

import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import grand.app.alamghareeb.base.BaseApp

object ResourceManager {
    fun getString(id: Int): String {
        return BaseApp.getInstance.resources.getString(id)
    }

    fun getColor(id: Int): Int? {
        return BaseApp.getInstance.resources?.getColor(id)
    }

    fun getDrawable(id: Int): Drawable? {
        return BaseApp.getInstance.let { ContextCompat.getDrawable(it, id) }
    }

    fun getDimens(id: Int): Float? {
        return BaseApp.getInstance.resources?.getDimension(id)
    }
}
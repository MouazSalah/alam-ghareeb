/*
package grand.app.salonssuser.utils.fcm;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tarashly.user.data.Codes;
import com.tarashly.user.utils.AppUtil;
import com.tarashly.user.utils.PrefMethods;
import com.tarashly.user.utils.fcm.FirebaseNotficationResponse;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import timber.log.Timber;

public class FCMService extends FirebaseMessagingService {
    private static final String MAIN_KEY = "message";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Timber.e("notification onMessageReceived");
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Timber.e("Message data payload: %s", remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            if (data.containsKey(MAIN_KEY)) {
                FirebaseNotficationResponse response = AppUtil.ObjectFromJson(data.get(MAIN_KEY), FirebaseNotficationResponse.class);
                NotificationUtil.sendNotification(response);
                sendIntentViaBroadcast(response);
                return;
            }
            //this for testing from firebase console
            NotificationUtil.sendNotification(data);
        }
        // Check if message contains a notification payload.
        else if (remoteMessage.getNotification() != null) {
            NotificationUtil.sendNotification(remoteMessage.getNotification());
        }
    }
    private void sendIntentViaBroadcast(FirebaseNotficationResponse response) {
        Intent intent = new Intent(); //used to receive in intent filter when register the broadcast;
        intent.setAction(Codes.HOME_PAGE);
        if(response.getNotificationType() == 3) {
            intent.putExtra(Codes.NOTIFICATION_RESPONSE, response);
        }else if (response.getNotificationType() == 2){

        }
        sendBroadcast(intent);
    }
    @Override
    public void onNewToken(@NotNull String token) {
        new PrefMethods().saveGoogleToken(token);
        Timber.e("Refreshed token: %s", token);
    }
}*/

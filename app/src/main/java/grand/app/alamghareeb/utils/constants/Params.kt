package grand.app.alamghareeb.utils.constants

object Params
{
    const val INTENT_PAGE_DIALOG = "INTENT_PAGE_DIALOG"
    const val BUNDLE_DIALOG = "BUNDLE_DIALOG"
    const val BRANCH_LOCATION = "BRANCH_LOCATION"
    const val BRANCH_LAT = "BRANCH_LAT"
    const val BRANCH_NAME = "BRANCH_NAME"
    const val BRANCH_LONG = "BRANCH_LONG"
    var categoryid: Int = -1
    var token: String? = null

    var brands = arrayListOf<String>()
    var sub_category_ids = arrayListOf<Int>()
    var min_price: Int? = null
    var max_price: Int? = null
    var parent_id: Int? = null
    var child_id: Int? = null

    const val DIALOG_CONFIRM_MESSAGE = "DIALOG_CONFIRM_MESSAGE"
    const val DIALOG_LOGIN_MESSAGE = "DIALOG_LOGIN_MESSAGE"
    const val DIALOG_CONFIRM_SUB_MESSAGE = "DIALOG_CONFIRM_SUB_MESSAGE"
    const val DIALOG_CONFIRM_POSITIVE = "DIALOG_CONFIRM_POSITIVE"
    const val DIALOG_CONFIRM_NEGATIVE = "DIALOG_CONFIRM_NEGATIVE"
    const val DIALOG_RATE_ORDER = "DIALOG_RATE_ORDER"
    const val ORDER_ID = "ORDER_ID"
    const val SORT_FLAG = "SORT_FLAG"
    const val SORT_ITEM = "SORT_ITEM"
    const val ORDER_Quantity = "ORDER_Quantity"
    const val REASONS_RESPONSE = "REASONS_RESPONSE"
    const val RESTORE_REASON = "RESTORE_REASON"

    const val DIALOG_CART_ITEM = "DIALOG_CART_ITEM"
    const val DIALOG_STORE_ITEM = "DIALOG_STORE_ITEM"
    const val DIALOG_ADDRESS_ITEM = "DIALOG_ADDRESS_ITEM"
    const val DIALOG_ORDER_ITEM = "DIALOG_ORDER_ITEM"

    const val DIALOG_CLICK_ACTION = "DIALOG_CLICK_ACTION"
    const val DELIVERY_ADDRESS_EMPTY = "DELIVERY_ADDRESS_EMPTY"

    const val DIALOG_TOAST_MESSAGE = "DIALOG_TOAST_MESSAGE"
    const val DIALOG_TOAST_TYPE = "DIALOG_TOAST_TYPE"
    const val DIALOG_ORDER_SUCCESS = "DIALOG_ORDER_SUCCESS"

    const val DIALOG_SHOW = "DIALOG_SHOW"
    const val DIALOG_SEARCH_FILTER = "DIALOG_SEARCH_FILTER"
    const val STORE_PRODUCT_ITEM = "STORE_PRODUCT_ITEM"
    const val STORE_CART_ITEM = "STORE_CART_ITEM"
    const val SALON_DETAILS_DATA = "SALON_DETAILS_DATA"
    const val ORDER_TIME = "ORDER_TIME"
    const val ORDER_DATE = "ORDER_DATE"
    const val ORDER_NOTE = "ORDER_NOTE"

    const val ALL_SALONS = "ALL_SALONS"

    const val ADDED_ADDRESS = "ADDED_ADDRESS"
    const val EDITED_ADDRESS = "EDITED_ADDRESS"
    const val ADDRESS_ITEM = "ADDRESS_ITEM"
    const val COUPON_ITEM = "COUPON_ITEM"

    const val DELIVERY_ADDRESSES_FLAG = "DELIVERY_ADDRESSES_FLAG"
    const val STORE_TIMES_RESPONSE = "STORE_TIMES_RESPONSE"
    const val DELIVERY_TIME = "DELIVERY_TIME"
    const val VERIFIED_PHONE = "VERIFIED_PHONE"
    const val VERIFIED_CODE = "VERIFIED_CODE"
    const val ADDRESS_ID = "ADDRESS_ID"
    const val PAYMENT_WAY = "PAYMENT_WAY"
    const val LANGUAGE = "LANGUAGE"
    const val ORDER_MSG = "ORDER_MSG"
    const val SERVICE_TYPE = "SERVICE_TYPE"
    const val FILTER_REQUEST = "FILTER_REQUEST"
    const val CITIES_RESPONSE = "CITIES_RESPONSE"
    const val AREAS_RESPONSE = "AREAS_RESPONSE"
    const val CITY_ITEM = "CITY_ITEM"
    const val AREA_ITEM = "AREA_ITEM"
    const val PROVIDER_TYPE = "PROVIDER_TYPE"
    const val RATE_MESSAGE = "RATE_MESSAGE"
    const val PRODUCT_ID = "PRODUCT_ID"
    const val ORDER_COST = "ORDER_COST"
    const val WALLET_AMOUNT = "WALLET_AMOUNT"
}
package grand.app.alamghareeb.base

import androidx.lifecycle.MutableLiveData
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LiveData {

    @Singleton
    @Provides
    fun getMutableLiveData(): MutableLiveData<Mutable?> {
        return MutableLiveData()
    }
}
package grand.app.alamghareeb.activity.main

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import grand.app.alamghareeb.base.BaseViewModel

class MainViewModel : BaseViewModel()
{
    val obsShowToolbar = ObservableBoolean()
    val obsShowBackBtn = ObservableBoolean()
    val obsShowSkipBtn = ObservableBoolean()
    val obsShowNotifyCount = ObservableBoolean()
    val obsTitle = ObservableField<String>()
    val obsNotifyCount = ObservableField<Int>()
    val obsProgressBar = ObservableField(false)

    val obsShowCartsCount = ObservableBoolean()
    val obsCartsCount = ObservableField<Int>()
}
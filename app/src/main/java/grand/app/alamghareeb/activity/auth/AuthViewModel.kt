package grand.app.alamghareeb.activity.auth

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import grand.app.alamghareeb.base.BaseViewModel

class AuthViewModel : BaseViewModel()
{
    val obsShowToolbar = ObservableBoolean()
    val obsShowBackBtn = ObservableBoolean()
    val obsTitle = ObservableField<String>()
    val obsProgressBar = ObservableField(false)
}
package grand.app.alamghareeb.activity.main

import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.BaseActivity
import grand.app.alamghareeb.databinding.ActivityMainBinding
import grand.app.alamghareeb.utils.PrefMethods

class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navView: BottomNavigationView
    lateinit var viewModel: MainViewModel
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this@MainActivity).get(MainViewModel::class.java)
        binding.viewModel = viewModel

        navView = findViewById(R.id.bottomBar)
        navController = findNavController(R.id.nav_home_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.navigation_offers, R.id.navigation_categories, R.id.navigation_home, R.id.navigation_carts, R.id.navigation_profile))
        navView.setupWithNavController(navController)

        updateCartBadge()

        binding.ibBack.setOnClickListener {
            when (navController.currentDestination?.id) {
                R.id.navigation_home -> {
                    finishAffinity()
                }
                else -> {
                    navController.navigateUp()
                }
            }
        }

        binding.cartLayout.setOnClickListener {
            navController.navigate(R.id.navigation_carts)
        }

        setUpToolbarAndStatusBar()

        binding.notificationLayout.setOnClickListener {
            navController.navigate(R.id.navigation_notifications)
        }
    }

    private fun setUpToolbarAndStatusBar() {
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_offers -> {
                    customToolBar(title = getString(R.string.title_offers),
                        showBackBtn = false,
                        showNotifyCount = true,
                        showBottomBar = true)
                }
                R.id.navigation_categories -> {
                    customToolBar(title = getString(R.string.title_categories),
                        showBackBtn = false,
                        showNotifyCount = false,
                        showBottomBar = true)
                }
                R.id.navigation_home -> {
                    customToolBar(title = getString(R.string.title_home),
                        showBackBtn = false,
                        showNotifyCount = true,
                        showBottomBar = true)
                }
                R.id.navigation_carts -> {
                    customToolBar(title = getString(R.string.title_cart),
                        showBackBtn = false,
                        showNotifyCount = true,
                        showBottomBar = true)
                }
                R.id.navigation_profile -> {
                    customToolBar(title = getString(R.string.title_profile),
                        showBackBtn = false,
                        showNotifyCount = true,
                        showBottomBar = true)
                }
                R.id.navigation_confirm_order -> {
                    customToolBar(title = getString(R.string.title_confirm_order),
                        showNotifyCount = true)
                }
                R.id.navigation_product_details -> {
                    customToolBar(title = getString(R.string.title_product_details),
                        showBackBtn = true,
                        showNotifyCount = false,
                        showCartsCount = true)
                }
                R.id.navigation_favorites -> {
                    customToolBar(title = getString(R.string.title_favorites))
                }
                R.id.navigation_help -> {
                    customToolBar(title = getString(R.string.title_help))
                }
                R.id.navigation_groups , R.id.navigation_products, R.id.navigation_top_rated, R.id.navigation_my_orders, R.id.navigation_about_app, R.id.navigation_order_details -> {
                    customToolBar(showToolBar = false)
                }
                R.id.navigation_notifications -> {
                    customToolBar(title = getString(R.string.title_notifications))
                }
                R.id.navigation_suggestions -> {
                    customToolBar(title = getString(R.string.title_suggestions))
                }
                R.id.navigation_addresses -> {
                    customToolBar(title = getString(R.string.title_addresses))
                }
                R.id.navigation_contact_us -> {
                    customToolBar(title = getString(R.string.title_contact_us))
                }
                R.id.navigation_wallet -> {
                    customToolBar(title = getString(R.string.title_wallet))
                }
                R.id.navigation_search -> {
                    customToolBar(title = getString(R.string.title_search))
                }
                R.id.navigation_rates -> {
                    customToolBar(title = getString(R.string.title_rates))
                }
                R.id.navigation_return_order -> {
                    customToolBar(title = getString(R.string.label_restore_order))
                }
                R.id.navigation_change_pass -> {
                    customToolBar(showToolBar = false)
                }
                R.id.navigation_edit_profile -> {
                    customToolBar(title = getString(R.string.title_edit_profile))
                }
                else -> {
                    customToolBar()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_home_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun customToolBar(
        title: String = "",
        showBottomBar: Boolean = false,
        showToolBar: Boolean = true,
        showSkip: Boolean = false,
        showBackBtn: Boolean = true,
        showNotifyCount: Boolean = false,
        showCartsCount: Boolean = false
    ) {
        viewModel.obsTitle.set(title)
        viewModel.obsShowToolbar.set(showToolBar)
        viewModel.obsShowSkipBtn.set(showSkip)
        viewModel.obsShowSkipBtn.set(showSkip)
        viewModel.obsShowCartsCount.set(showCartsCount)
        if (showCartsCount == true) {
            when {
                PrefMethods.getUserCarts() == null || PrefMethods.getUserCarts()?.size == 0 -> {
                    viewModel.obsCartsCount.set(0)
                }
                else -> {
                    viewModel.obsCartsCount.set(PrefMethods.getUserCarts()?.size!!)
                }
            }
        }

        isLogin(showNotifyCount)

        viewModel.obsShowBackBtn.set(showBackBtn)

        when (showBottomBar) {
            false -> {
                binding.bottomBar.visibility = View.GONE
            }
            else -> {
                binding.bottomBar.visibility = View.VISIBLE
            }
        }

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }
    }

    private fun isLogin(showNotifyCount: Boolean = false) {
        viewModel.obsShowNotifyCount.set(showNotifyCount)

        when {
            PrefMethods.getUserData() == null -> {
                viewModel.obsNotifyCount.set(0)
            }
            PrefMethods.getUserData()?.notificationCount == 0 -> {
                viewModel.obsNotifyCount.set(0)
            }
            else -> {
                viewModel.obsNotifyCount.set(PrefMethods.getUserData()?.notificationCount)
            }
        }
    }

    fun updateCartBadge()
    {
        val badge = navView.getOrCreateBadge(R.id.navigation_carts)
        when {
            PrefMethods.getUserCarts() == null || PrefMethods.getUserCarts()?.size == 0 -> {
                badge.number = 0
                badge.isVisible = false
            }
            else -> {
                badge.isVisible = true
                badge.number = PrefMethods.getUserCarts()?.size!!
            }
        }
    }
}
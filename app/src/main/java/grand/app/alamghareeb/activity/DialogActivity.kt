package grand.app.alamghareeb.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import grand.app.alamghareeb.R
import grand.app.alamghareeb.utils.LocalUtil
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Params
import timber.log.Timber

class DialogActivity : AppCompatActivity()
{
    var isProgressShow = SingleLiveEvent<Boolean>()

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocalUtil.onAttach(newBase))
    }

    private fun addFragment(fragmentString: String?, bundle: Bundle?) {
        try {
            val fragment = Class.forName(fragmentString)
                .newInstance() as Fragment
            fragment.arguments = bundle
            Utils.replaceFragment(this, fragment, "")
        } catch (e: IllegalAccessException) {
            Timber.e("Fragment Not Found \n %s", e)
        } catch (e: InstantiationException) {
            Timber.e("Fragment Not Found \n %s", e)
        } catch (e: ClassNotFoundException) {
            Timber.e("Fragment Not Found \n %s", e)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        LocalUtil.changeLanguage(this)
        super.onCreate(savedInstanceState)
        LocalUtil.changeLanguage(this)
        setContentView(R.layout.activity_dialog)
        if (intent.hasExtra(Params.INTENT_PAGE_DIALOG)) {
            addFragment(
                intent.getStringExtra(Params.INTENT_PAGE_DIALOG),
                intent.getBundleExtra(Params.BUNDLE_DIALOG)
            )
        }
    }
}
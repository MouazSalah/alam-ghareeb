package grand.app.alamghareeb.dialogs.couponapplied

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogCouponAppliedBinding
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params

class DialogCouponAppliedFragment  : DialogFragment()
{
    lateinit  var  binding: DialogCouponAppliedBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogCouponAppliedBinding.inflate(inflater, container, false)

        binding.tvMessage.text = getString(R.string.label_coupon_applied_successfully)

        binding.layoutBooked.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_COUPON_APPLIED, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}
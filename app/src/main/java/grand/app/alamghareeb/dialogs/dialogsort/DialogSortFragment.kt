package grand.app.alamghareeb.dialogs.dialogsort

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogSortBinding
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe

class DialogSortFragment  : DialogFragment()
{
    lateinit  var  binding: DialogSortBinding
    lateinit var viewModel : DialogSortViewModel
    var sortFlag : Int = 1

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.SORT_FLAG) -> {
                        sortFlag = requireArguments().getInt(Params.SORT_FLAG, 0)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogSortBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(DialogSortViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.appendList(sortFlag)

        observe(viewModel.adapter.itemLiveData){
            when {
                it != null -> {
                    viewModel.sortItem = it
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_SELECTED_SORT_WAY -> {
                    showToast(getString(R.string.msg_empty_selected_sort_way), 1)
                }
                Codes.SELECT_SORT_WAY -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.SORT_ITEM, viewModel.sortItem)
                    requireActivity().setResult(Codes.DIALOG_SORT_ORDERS, intent)
                    requireActivity().finish()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_SORT_ORDERS, intent)
                    requireActivity().finish()
                }
            }
        }
    }

    fun showToast(msg : String, type : Int) {
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(requireActivity(), DialogToastFragment::class.java.name, Codes.DIALOG_TOAST_REQUEST, bundle)
    }
}
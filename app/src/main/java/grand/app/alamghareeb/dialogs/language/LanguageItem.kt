package grand.app.alamghareeb.dialogs.language

data class LanguageItem(
    var id: Int? = null,
    var name: String? = null,
    var value: String? = null
)

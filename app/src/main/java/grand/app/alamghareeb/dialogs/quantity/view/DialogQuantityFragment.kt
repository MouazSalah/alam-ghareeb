package grand.app.alamghareeb.dialogs.quantity.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogQuantityBinding
import grand.app.alamghareeb.dialogs.quantity.viewmodel.DialogQuantityViewModel
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe

class DialogQuantityFragment  : DialogFragment()
{
    lateinit  var  binding: DialogQuantityBinding
    lateinit var viewModel : DialogQuantityViewModel
    var quantity : Int ?= null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.ORDER_Quantity) -> {
                        quantity = requireArguments().getInt(Params.ORDER_Quantity)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogQuantityBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(DialogQuantityViewModel::class.java)
        binding.viewModel = viewModel

        observe(viewModel.adapter.itemLiveData){
            when {
                it != null -> {
                    viewModel.quantity = it
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_SELECTED_SORT_WAY -> {
                    showToast(getString(R.string.msg_empty_selected_quantity), 1)
                }
                Codes.SELECT_SORT_WAY -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.ORDER_Quantity, viewModel.quantity)
                    requireActivity().setResult(Codes.DIALOG_RESTORE_QUANTITY, intent)
                    requireActivity().finish()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_RESTORE_QUANTITY, intent)
                    requireActivity().finish()
                }
            }
        }
    }

    fun showToast(msg : String, type : Int) {
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(requireActivity(), DialogToastFragment::class.java.name, Codes.DIALOG_RESTORE_QUANTITY, bundle)
    }
}
package grand.app.alamghareeb.dialogs.dialogsort

import java.io.Serializable

data class SortItem(
    var id: Int? = null,
    var name: String? = null,
    var value: String? = null
) : Serializable

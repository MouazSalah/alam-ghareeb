package grand.app.alamghareeb.dialogs.addrate.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogAddRateBinding
import grand.app.alamghareeb.dialogs.addrate.model.AddRateResponse
import grand.app.alamghareeb.dialogs.addrate.viewmodel.DialogAddRateViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class DialogAddRateFragment  : DialogFragment()
{
    lateinit  var  binding: DialogAddRateBinding
    lateinit var viewModel : DialogAddRateViewModel
    var productId : Int = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.PRODUCT_ID) -> {
                        productId = requireArguments().getInt(Params.PRODUCT_ID, 0)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogAddRateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogAddRateViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.id = productId

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_MESSAGE -> Toasty.error(requireActivity(), getString(R.string.msg_empty_message)).show()
            }
        }

        binding.btnIgnore.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
            requireActivity().setResult(Codes.DIALOG_RATE_ORDER, intent)
            requireActivity().finish()
        }

      binding.btnCancel.setOnClickListener {
                val intent = Intent()
                intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                requireActivity().setResult(Codes.DIALOG_RATE_ORDER, intent)
                requireActivity().finish()
            }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    Toasty.error(requireActivity(), it.message.toString()).show()
                }
                Status.SUCCESS_MESSAGE -> {
                    Toasty.success(requireActivity(), it.message.toString()).show()
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddRateResponse -> {
                            val intent = Intent()
                            intent.putExtra(Params.RATE_MESSAGE, it.data.message)
                            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                            requireActivity().setResult(Codes.DIALOG_RATE_ORDER, intent)
                            requireActivity().finish()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}
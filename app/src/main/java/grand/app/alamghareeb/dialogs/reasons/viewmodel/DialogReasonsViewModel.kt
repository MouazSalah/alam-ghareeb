package grand.app.alamghareeb.dialogs.reasons.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.dialogs.dialogsort.SortItem
import grand.app.alamghareeb.dialogs.reasons.view.ReasonsAdapter
import grand.app.alamghareeb.main.restoreorder.model.ReasonItem
import grand.app.alamghareeb.utils.constants.Codes

class DialogReasonsViewModel : BaseViewModel()
{
    var adapter = ReasonsAdapter()
    var reasonItem = ReasonItem()

    fun onConfirmClicked() {
        when (adapter.selectedPosition) {
            -1 -> {
                setValue(Codes.EMPTY_SELECTED_REASON)
            }
            else -> {
                setValue(Codes.SELECT_SORT_WAY)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}
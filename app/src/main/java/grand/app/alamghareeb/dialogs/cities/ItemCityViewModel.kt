package grand.app.alamghareeb.dialogs.cities

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.addresses.model.CityItem

class ItemCityViewModel(var item: CityItem) : BaseViewModel()

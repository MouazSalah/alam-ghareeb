package grand.app.alamghareeb.dialogs.dialogsort

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawFilterOrdersBinding
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class SortAdapter : RecyclerView.Adapter<SortAdapter.PaymentHolder>()
{
    var itemsList: ArrayList<SortItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SortItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawFilterOrdersBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_filter_orders, parent, false)
        return PaymentHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        val itemViewModel = ItemSortViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): SortItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SortItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PaymentHolder(val binding: RawFilterOrdersBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}

package grand.app.alamghareeb.dialogs.dialogsort

import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.utils.constants.Codes

class DialogSortViewModel : BaseViewModel()
{
    var adapter = SortAdapter()
    var sortItem = SortItem()

    fun appendList(flag : Int ){
        val sortList1 = arrayListOf(
            SortItem(0 , getString(R.string.label_last_three_months), "last_3_months"),
            SortItem(1 , getString(R.string.label_last_six_months), "last_6_months"),
            SortItem(2 , getString(R.string.label_last_nine_months), "last_9_months")
        )

        val sortList2 = arrayListOf(
            SortItem(0 , getString(R.string.label_top_rated), "highest_rate"),
            SortItem(1 , getString(R.string.label_minimum_price), "lowest_price"),
            SortItem(2 , getString(R.string.label_maximum_price), "highest_price")
        )

        when (flag) {
            Codes.SORT_ORDERS -> {
                adapter.updateList(sortList1)
            }
            Codes.SORT_PRODUCTS -> {
                adapter.updateList(sortList2)
            }
        }
    }

    fun onFilterClicked() {
        when (adapter.selectedPosition) {
            -1 -> {
                setValue(Codes.EMPTY_SELECTED_SORT_WAY)
            }
            else -> {
                setValue(Codes.SELECT_SORT_WAY)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }

    fun onClearClicked() {
        adapter.selectedPosition = -1
        adapter.notifyDataSetChanged()
    }
}
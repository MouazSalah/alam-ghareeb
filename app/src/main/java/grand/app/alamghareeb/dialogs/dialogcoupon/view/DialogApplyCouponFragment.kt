package grand.app.alamghareeb.dialogs.dialogcoupon.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.alamghareeb.databinding.DialogApplyCouponBinding
import grand.app.alamghareeb.dialogs.dialogcoupon.model.ApplyCouponResponse
import grand.app.alamghareeb.dialogs.dialogcoupon.viewmodel.DialogApplyCouponViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class DialogApplyCouponFragment  : DialogFragment()
{
    lateinit  var  binding: DialogApplyCouponBinding
    lateinit var viewModel : DialogApplyCouponViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogApplyCouponBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogApplyCouponViewModel::class.java)
        binding.viewModel = viewModel

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    Toasty.error(requireActivity(), it.message.toString()).show()
                }
                Status.SUCCESS_MESSAGE -> {
                    Toasty.success(requireActivity(), it.message.toString()).show()
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ApplyCouponResponse -> {
                            val intent = Intent()
                            intent.putExtra(Params.COUPON_ITEM, it.data.applyCouponData)
                            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                            requireActivity().setResult(Codes.DIALOG_APPLY_COUPON, intent)
                            requireActivity().finish()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}
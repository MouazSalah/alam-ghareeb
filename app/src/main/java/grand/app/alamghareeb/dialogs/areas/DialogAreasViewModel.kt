package grand.app.alamghareeb.dialogs.areas

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.dialogs.cities.CitiesAdapter
import grand.app.alamghareeb.dialogs.language.LanguageAdapter
import grand.app.alamghareeb.dialogs.language.LanguageItem
import grand.app.alamghareeb.main.addresses.model.AreaItem
import grand.app.alamghareeb.main.addresses.model.CityItem
import grand.app.alamghareeb.utils.constants.Codes

class DialogAreasViewModel : BaseViewModel()
{
    var adapter = AreasAdapter()
    var areaItem = AreaItem()

    fun onConfirmClicked() {
        when (adapter.selectedPosition) {
            -1 -> {
                setValue(Codes.EMPTY_SELECTED_AREA)
            }
            else -> {
                setValue(Codes.SELECT_AREA)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}
package grand.app.alamghareeb.dialogs.quantity.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawQuantityBinding
import grand.app.alamghareeb.dialogs.quantity.viewmodel.ItemQuantityViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class QuantityAdapter : RecyclerView.Adapter<QuantityAdapter.QuantityHolder>()
{
    var itemsList: ArrayList<Int> = ArrayList()
    var itemLiveData = SingleLiveEvent<Int>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuantityHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawQuantityBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_quantity, parent, false)
        return QuantityHolder(binding)
    }

    override fun onBindViewHolder(holder: QuantityHolder, position: Int) {
        val itemViewModel = ItemQuantityViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): Int {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<Int>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class QuantityHolder(val binding: RawQuantityBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}

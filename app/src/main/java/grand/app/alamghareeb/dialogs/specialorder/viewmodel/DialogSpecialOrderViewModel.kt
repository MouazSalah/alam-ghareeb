package grand.app.alamghareeb.dialogs.specialorder.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.auth.register.model.RegisterResponse
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.dialogs.specialorder.model.SpecialOrderRequest
import grand.app.alamghareeb.dialogs.specialorder.model.SpecialOrderResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import grand.app.alamghareeb.utils.stringPathToFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class DialogSpecialOrderViewModel : BaseViewModel()
{
    var request = SpecialOrderRequest()
    var obsRate = ObservableField<Float>(5f)
    val obsPrice = ObservableField<Double>()

    fun onPlusClicked() {
        request.quantity = request.quantity.plus(1)
        notifyChange()
    }

    fun onMinusClicked() {
        when {
            request.quantity != 1 -> {
                request.quantity = request.quantity.minus(1)
            }
        }
        notifyChange()
    }

    fun gotImage(requestCode: Int, path: String) {
        when (requestCode) {
            Codes.PICK_ORDER_IMAGE -> {
                request.product_image = path.stringPathToFile()
                notifyChange()
            }
        }
    }

    fun onSendClicked() {
        when {
            request.name == null -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_product_name))
            }
            request.price == null -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_product_price))
            }
            else -> {
                makeOrder()
            }
        }
    }

    private fun makeOrder() {
        obsIsProgress.set(true)

        viewModelScope.launch {
            val res = getApiRepo().specialOrder(request)
            obsIsProgress.set(false)
            when (res.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.message)
                }
            }
        }
    }

    fun onUploadImgClicked() {
        setValue(Codes.PICK_ORDER_IMAGE)
    }
}
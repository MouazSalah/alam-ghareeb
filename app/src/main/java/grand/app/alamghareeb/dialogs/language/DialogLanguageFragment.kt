package grand.app.alamghareeb.dialogs.language

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import grand.app.alamghareeb.databinding.DialogLanguageBinding
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class DialogLanguageFragment  : DialogFragment()
{
    lateinit  var  binding: DialogLanguageBinding
    lateinit var viewModel : DialogLanguageViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogLanguageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogLanguageViewModel::class.java)
        binding.viewModel = viewModel

        observe(viewModel.adapter.itemLiveData){
            viewModel.language = it?.value
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.SELECT_LANGUAGE -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    Timber.e("mou3az_language : " + viewModel.language)
                    intent.putExtra(Params.LANGUAGE, viewModel.language)
                    requireActivity().setResult(Codes.DIALOG_SELECT_LANGUAGE, intent)
                    requireActivity().finish()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_SELECT_LANGUAGE, intent)
                    requireActivity().finish()
                }
            }
        }
    }
}
package grand.app.alamghareeb.dialogs.addrate.model

import com.google.gson.annotations.SerializedName

data class AddRateResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Any? = null,

	@field:SerializedName("message")
	val message: String? = null
)

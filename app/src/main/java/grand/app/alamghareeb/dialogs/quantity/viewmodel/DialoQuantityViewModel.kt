package grand.app.alamghareeb.dialogs.quantity.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.dialogs.quantity.view.QuantityAdapter
import grand.app.alamghareeb.utils.constants.Codes

class DialogQuantityViewModel : BaseViewModel()
{
    var adapter = QuantityAdapter()
    var quantity : Int ?= null

    fun onConfirmClicked() {
        when (adapter.selectedPosition) {
            -1 -> {
                setValue(Codes.EMPTY_SELECTED_QUANATIY)
            }
            else -> {
                setValue(Codes.QUANTTY_SELECTED)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}
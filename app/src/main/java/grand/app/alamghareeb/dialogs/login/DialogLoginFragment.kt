package grand.app.alamghareeb.dialogs.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.alamghareeb.databinding.DialogConfirmBinding
import grand.app.alamghareeb.databinding.DialogLoginBinding
import grand.app.alamghareeb.databinding.FragmentLoginBinding
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params

class DialogLoginFragment  : DialogFragment()
{
    lateinit  var  binding: DialogLoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogLoginBinding.inflate(inflater, container, false)

        binding.btnConfirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_LOGIN_REQUEST, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}
package grand.app.alamghareeb.dialogs.dialogcoupon.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.dialogs.dialogcoupon.model.ApplyCouponResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall

class DialogApplyCouponViewModel : BaseViewModel()
{
    var obsCoupon = ObservableField<String>()

    fun onApplyClicked() {
        when {
            obsCoupon.get() == null || obsCoupon.get() == "" -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.label_please_enter_promo_code))
            }
            else -> {
                applyCoupon()
            }
        }
    }

    fun applyCoupon() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().applyCoupon(URLS.APPLY_COUPON, obsCoupon.get())}) { res ->
            obsIsProgress.set(false)
            val response : ApplyCouponResponse = Gson().fromJson(res, ApplyCouponResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}
package grand.app.alamghareeb.dialogs.reasons.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogReasonsBinding
import grand.app.alamghareeb.dialogs.reasons.viewmodel.DialogReasonsViewModel
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.main.restoreorder.model.ReasonItem
import grand.app.alamghareeb.main.restoreorder.model.ReasonsResponse
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe

class DialogReasonsFragment  : DialogFragment()
{
    lateinit  var  binding: DialogReasonsBinding
    lateinit var viewModel : DialogReasonsViewModel
    var reasonsResponse = ReasonsResponse()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.REASONS_RESPONSE) -> {
                        reasonsResponse = requireArguments().getSerializable(Params.REASONS_RESPONSE) as ReasonsResponse
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogReasonsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(DialogReasonsViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.adapter.updateList(reasonsResponse.reasonsList as ArrayList<ReasonItem>)

        observe(viewModel.adapter.itemLiveData){
            when {
                it != null -> {
                    viewModel.reasonItem = it
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.EMPTY_SELECTED_REASON -> {
                    showToast(getString(R.string.msg_choose_restore_reason), 1)
                }
                Codes.SELECT_SORT_WAY -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.RESTORE_REASON, viewModel.reasonItem.name)
                    requireActivity().setResult(Codes.DIALOG_RESTORE_REASONS, intent)
                    requireActivity().finish()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_RESTORE_REASONS, intent)
                    requireActivity().finish()
                }
            }
        }
    }

    fun showToast(msg : String, type : Int) {
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(requireActivity(), DialogToastFragment::class.java.name, Codes.DIALOG_RESTORE_REASONS, bundle)
    }
}
package grand.app.alamghareeb.dialogs.wallet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogWalletBinding
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import java.lang.StringBuilder

class DialogWalletFragment  : DialogFragment()
{
    lateinit  var  binding: DialogWalletBinding
    var orderCost : Double ?= null
    var walletAmount : Double ?= null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.ORDER_COST) -> {
                        orderCost = requireArguments().getDouble(Params.ORDER_COST)
                        walletAmount = requireArguments().getDouble(Params.WALLET_AMOUNT)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        binding = DialogWalletBinding.inflate(inflater, container, false)

        val title = StringBuilder()
        title.append(getString(R.string.label_order_cost))
        title.append("(" + orderCost + getString(R.string.label_currency)+ ")")
        title.append(getString(R.string.label_cost_wallet))
        title.append("(" + walletAmount + getString(R.string.label_currency)+ ")")
        title.append(getString(R.string.label_pay_in_cash))

        binding.tvMessage.text = title.toString()

        binding.btnIgnore.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
            requireActivity().setResult(Codes.DIALOG_CONFIRM_REQUEST, intent)
            requireActivity().finish()
        }

        binding.btnConfirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_CONFIRM_REQUEST, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}
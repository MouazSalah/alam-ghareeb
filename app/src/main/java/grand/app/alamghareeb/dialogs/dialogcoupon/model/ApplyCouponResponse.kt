package grand.app.alamghareeb.dialogs.dialogcoupon.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ApplyCouponResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val applyCouponData: ApplyCouponData? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class ApplyCouponData(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("promo_type")
	val promoType: String? = null
) : Serializable

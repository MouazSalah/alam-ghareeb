package grand.app.alamghareeb.main.confirmorder.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentConfirmOrderBinding
import grand.app.alamghareeb.dialogs.couponapplied.DialogCouponAppliedFragment
import grand.app.alamghareeb.dialogs.dialogcoupon.model.ApplyCouponData
import grand.app.alamghareeb.dialogs.dialogcoupon.view.DialogApplyCouponFragment
import grand.app.alamghareeb.dialogs.orderbooked.DialogOrderBookedFragment
import grand.app.alamghareeb.dialogs.wallet.DialogWalletFragment
import grand.app.alamghareeb.main.addaddress.view.AddAddressActivity
import grand.app.alamghareeb.main.confirmorder.model.ConfirmOrderResponse
import grand.app.alamghareeb.main.confirmorder.viewmodel.ConfirmOrderViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class ConfirmOrderFragment : BaseFragment()
{
    lateinit var binding: FragmentConfirmOrderBinding
    lateinit var viewModel: ConfirmOrderViewModel
    val fragmentArgs : ConfirmOrderFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_order, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ConfirmOrderViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.branchId = fragmentArgs.branchId
        when (fragmentArgs.branchId) {
            -1 -> {
                binding.deliveryLayout.visibility = View.VISIBLE
            }
            else -> {
                binding.deliveryLayout.visibility = View.GONE
            }
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.ADD_ADDRESS_CLICKED -> {
                    requireActivity().startActivityForResult(Intent(requireActivity(),
                        AddAddressActivity::class.java), Codes.ADD_ADDRESS_REQUEST_CODE)
                }
            }
        }

        binding.walletLayout.setOnClickListener {
            when (viewModel.isWallet) {
                0 -> {
                    when {
                        viewModel.walletCredit == 0.0 -> {
                            showToast(getString(R.string.zero_wallet_credit), 1)
                        }
                        viewModel.walletCredit != 0.0 && viewModel.walletCredit < viewModel.totalCost -> {
                            val bundle = Bundle()
                            bundle.putDouble(Params.WALLET_AMOUNT, viewModel.walletCredit)
                            bundle.putDouble(Params.ORDER_COST, viewModel.totalCost)
                            Utils.startDialogActivity(requireActivity(),
                                DialogWalletFragment::class.java.name,
                                Codes.DIALOG_WALLET_REQUEST,
                                bundle)
                        }
                        else -> {
                            viewModel.isWallet = 1
                            binding.ibWallet.setImageResource(R.drawable.ic_checked)
                        }
                    }
                }
                1 -> {
                    viewModel.isWallet = 0
                    binding.ibWallet.setImageResource(R.drawable.ic_unchecked)
                }
            }
        }

        when {
            fragmentArgs.branchId != -1 -> {
                viewModel.orderRequest.branch_id = fragmentArgs.branchId
                viewModel.orderRequest.pay_type = "branch"
                binding.addressLayout.visibility = View.GONE
            }
            else -> {
                viewModel.orderRequest.pay_type = "home"
                binding.addressLayout.visibility = View.VISIBLE
            }
        }

        observe(viewModel.addressesAdapter.itemLiveData){
            when {
                viewModel.productsCost > 2000 -> {
                    viewModel.deliveryCost = 0.0
                }
                else -> {
                    viewModel.deliveryCost = it?.deliveryCost?.toDouble()!!
                    viewModel.obsDeliveryCost.set(" ${viewModel.deliveryCost} ${getString(R.string.label_currency)}")
                    viewModel.totalCost = viewModel.deliveryCost + viewModel.totalAfterDiscount
                    viewModel.obsTotalPrice.set(" ${viewModel.totalCost} ${getString(R.string.label_currency)}")
                    viewModel.orderRequest.address_id = it.id
                    viewModel.orderRequest.address = it.name
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ConfirmOrderResponse -> {
                            (requireActivity() as MainActivity).updateCartBadge()
                            Utils.startDialogActivity(requireActivity(),
                                DialogOrderBookedFragment::class.java.name,
                                Codes.DIALOG_ORDER_SUCCESS,
                                null)
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.APPLY_COUPON_CLICKED -> {
                                    Utils.startDialogActivity(requireActivity(),
                                        DialogApplyCouponFragment::class.java.name,
                                        Codes.DIALOG_APPLY_COUPON,
                                        null)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_WALLET_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.isWallet = 1
                                        binding.ibWallet.setImageResource(R.drawable.ic_checked)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_APPLY_COUPON -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        val couponResponse =  data.getSerializableExtra(Params.COUPON_ITEM) as ApplyCouponData
                                        viewModel.totalAfterDiscount = viewModel.productsCost - couponResponse.amount!!
                                        viewModel.obsAfterDiscountCost.set("${viewModel.totalAfterDiscount} ${getString(R.string.label_currency)}")

                                        viewModel.totalCost = viewModel.totalAfterDiscount + viewModel.deliveryCost
                                        viewModel.obsTotalPrice.set("${viewModel.totalCost} ${getString(R.string.label_currency)}")
                                        Utils.startDialogActivity(requireActivity(), DialogCouponAppliedFragment::class.java.name, Codes.DIALOG_COUPON_APPLIED, null)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.ADD_ADDRESS_REQUEST_CODE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.getAllAddresses()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_ORDER_SUCCESS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java))
                                        requireActivity().finishAffinity()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
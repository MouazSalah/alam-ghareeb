package grand.app.alamghareeb.main.help.viewmodel

import androidx.core.content.ContextCompat
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.help.view.ItemHelpModel
import grand.app.alamghareeb.main.help.view.HelpAdapter

class HelpViewModel : BaseViewModel()
{
    var adapter = HelpAdapter()

    init {
        val moreList = arrayListOf(
                ItemHelpModel(0, getString(R.string.label_rate_app), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_rate)),
                ItemHelpModel(1, getString(R.string.label_share), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_share)),
                ItemHelpModel(2, getString(R.string.title_suggestions), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable. ic_suggest)),
                ItemHelpModel(3, getString(R.string.terms_conditions), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_term)),
                ItemHelpModel(4, getString(R.string.title_privacy_policy), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_compliant)),
                ItemHelpModel(5, getString(R.string.contact_us), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_contact_us)),
                ItemHelpModel(6, getString(R.string.title_about_app), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_about_app)),
                ItemHelpModel(7, getString(R.string.title_change_language), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_language))
        )

        adapter.updateList(moreList)
    }
}
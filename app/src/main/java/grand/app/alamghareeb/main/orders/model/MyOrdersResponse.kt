package grand.app.alamghareeb.main.orders.model

import com.google.gson.annotations.SerializedName

data class MyOrdersResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val myOrdersData: MyOrderData? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class Meta(

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)

data class OrderProductItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("quantity")
    val quantity: Int? = null,

    @field:SerializedName("color")
    val color: String? = null,

    @field:SerializedName("size")
    val size: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("pay_type")
    val pay_type: String? = null
)

data class MyOrderData(

	@field:SerializedName("data")
	val myOrdersList: List<MyOrderItem?>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null,

	@field:SerializedName("links")
	val links: Links? = null
)

data class MyOrderItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("pay_type")
    val payType: String? = null,

    @field:SerializedName("history")
    val historyList: List<HistoryItem?>? = null,

    @field:SerializedName("products")
    val productsList: List<OrderProductItem?>? = null
)

data class HistoryItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Links(

	@field:SerializedName("next")
	val next: Any? = null,

	@field:SerializedName("last")
	val last: String? = null,

	@field:SerializedName("prev")
	val prev: Any? = null,

	@field:SerializedName("first")
	val first: String? = null
)

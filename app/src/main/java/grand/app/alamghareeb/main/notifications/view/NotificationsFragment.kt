package grand.app.alamghareeb.main.notifications.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import grand.app.alamghareeb.main.notifications.viewmodel.NotificationsViewModel
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentNotificationsBinding
import grand.app.alamghareeb.main.notifications.model.NotificationsResponse
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class NotificationsFragment : BaseFragment()
{
    lateinit var binding: FragmentNotificationsBinding
    lateinit var viewModel: NotificationsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(NotificationsViewModel::class.java)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getNotifications()
        }

        observe(viewModel.adapter.itemLiveData) {

        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is NotificationsResponse -> {
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.LOGIN_CLICKED -> {
                                    Const.isAskedToLogin = 1
                                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}
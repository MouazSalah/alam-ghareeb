package grand.app.alamghareeb.main.productdetails.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentProductDetailsBinding
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.productdetails.model.ProductColorItem
import grand.app.alamghareeb.main.productdetails.model.ProductDetailsResponse
import grand.app.alamghareeb.main.productdetails.model.ProductSizeItem
import grand.app.alamghareeb.main.productdetails.model.ProductSliderItem
import grand.app.alamghareeb.main.productdetails.viewmodel.ProductDetailsViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class ProductDetailsFragment : BaseFragment()
{
    lateinit var binding: FragmentProductDetailsBinding
    @Inject lateinit var viewModel: ProductDetailsViewModel
    private val fragmentArgs : ProductDetailsFragmentArgs by navArgs()
    var productItem = ProductItem()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getProductDetails(fragmentArgs.productId)
        }

        viewModel.getProductDetails(fragmentArgs.productId)

        observe(viewModel.colorsAdapter.itemLiveData) {
            viewModel.colorItem = it!!
            when (viewModel.colorsAdapter.selectedPosition) {
                -1 -> {
                    binding.sizeLayout.visibility = View.GONE
                    binding.tvMainPrice.text = viewModel.productDetails.price.toString()
                    viewModel.productSize = viewModel.productDetails.price
                }
                else -> {
                    binding.colorLayout.visibility = View.VISIBLE
                    when (it.sizesList?.size) {
                        0 -> {
                            binding.sizeLayout.visibility = View.GONE
                            binding.tvMainPrice.text = viewModel.productDetails.price.toString()
                            viewModel.productSize = viewModel.productDetails.price
                        }
                        else -> {
                            binding.sizeLayout.visibility = View.VISIBLE
                            viewModel.sizesAdapter.updateList(it.sizesList as ArrayList<ProductSizeItem>)
                        }
                    }
                }
            }
        }

        observe(viewModel.sizesAdapter.itemLiveData) {
            viewModel.sizeItem = it!!
            binding.tvMainPrice.text = it.price.toString()
            viewModel.productSize = it.price
        }

        observe(viewModel.relatedProductsAdapter.favLiveData){
            it?.let { item ->
                when {
                    PrefMethods.getUserData() == null -> {
                        productItem = it
                        Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                    }
                    else -> {
                        viewModel.addToFav(item.id!!)
                    }
                }
            }
        }

        observe(viewModel.relatedProductsAdapter.cartLiveData){
            it?.let { item ->
                when {
                    PrefMethods.getUserData() == null -> {
                        productItem = it
                        Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                    }
                    else -> {
                        viewModel.getProductDetails(item.id!!)
                    }
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ProductDetailsResponse -> {
                            when (it.data.productDetailsData?.brand) {
                                null -> {
                                    binding.tvBrandValue.text = it.data.productDetailsData?.brand
                                }
                                else -> {
                                    binding.tvBrandValue.text = getString(R.string.label_not_found)
                                }
                            }

                            when (it.data.productDetailsData?.weight) {
                                null -> {
                                    binding.tvWeightValue.text = it.data.productDetailsData?.weight
                                }
                                else -> {
                                    binding.tvWeightValue.text = getString(R.string.label_not_found)
                                }
                            }

                            when (it.data.productDetailsData?.type) {
                                null -> {
                                    binding.tvTypeValue.text = it.data.productDetailsData?.type
                                }
                                else -> {
                                    binding.tvTypeValue.text = getString(R.string.label_not_found)
                                }
                            }


                            when (it.data.productDetailsData?.serial) {
                                null -> {
                                    Timber.e("mou3az_product_deails 11 : " + it.data.productDetailsData?.serial)
                                    binding.tvSerialValue.text = it.data.productDetailsData?.serial
                                }
                                else -> {
                                    Timber.e("mou3az_product_deails 22 : " + it.data.productDetailsData?.serial)
                                    binding.tvSerialValue.text = getString(R.string.label_not_found)
                                }
                            }

                            when (it.data.productDetailsData?.validityPeriod) {
                                null -> {
                                    binding.tvValidityValue.text =
                                        it.data.productDetailsData?.validityPeriod
                                }
                                else -> {
                                    binding.tvValidityValue.text =
                                        getString(R.string.label_not_found)
                                }
                            }

                            setupSlider(it.data.productDetailsData?.productImagesList as ArrayList<ProductSliderItem>)
                            when (it.data.productDetailsData.colorsList?.size) {
                                0 -> {
                                    binding.colorLayout.visibility = View.GONE
                                    binding.sizeLayout.visibility = View.GONE
                                }
                                else -> {
                                    binding.colorLayout.visibility = View.VISIBLE
                                    viewModel.colorsAdapter.updateList(it.data.productDetailsData.colorsList as java.util.ArrayList<ProductColorItem>)

                                    when (it.data.productDetailsData.colorsList[0].sizesList?.size) {
                                        0 -> {
                                            binding.sizeLayout.visibility = View.GONE
                                        }
                                        else -> {
                                            binding.sizeLayout.visibility = View.VISIBLE
                                            viewModel.sizesAdapter.updateList(it.data.productDetailsData.colorsList[0].sizesList as java.util.ArrayList<ProductSizeItem>)
                                        }
                                    }
                                }
                            }
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.SHARE_APP_CLICKED -> {
                                    Utils.shareApp(requireActivity(),
                                        "تطبيق عالم غريب ",
                                        "https://play.google.com/store/apps/details?id=com.attar.app")
                                }
                                Codes.RATE_ORDER_CLICKED -> {
                                    findNavController().navigate(ProductDetailsFragmentDirections.productDetailsToRates(
                                        fragmentArgs.productId))
                                }
                                Codes.ADDED_TO_CART -> {
                                    (requireActivity() as MainActivity).updateCartBadge()
                                    findNavController().navigate(R.id.navigation_carts)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(
                                            Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 && PrefMethods.getUserData() != null -> {
                Const.isAskedToLogin = 0
                viewModel.getProductDetails(fragmentArgs.productId)
            }
        }
    }

    private fun setupSlider(covers: ArrayList<ProductSliderItem>) {
        viewModel.sliderAdapter.updateList(covers)
        val sliderView = binding.sliderProductDetails
        sliderView.setSliderAdapter(viewModel.sliderAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }
}
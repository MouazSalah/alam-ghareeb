package grand.app.alamghareeb.main.restoreorder.viewmodel

import androidx.databinding.ObservableField
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import grand.app.alamghareeb.utils.resources.ResourceManager

class ItemRestoreProductViewModel(var item: OrderProductItem) : BaseViewModel()
{
    var obsPrice = ObservableField<String>()

    init {
        obsPrice.set("${item.total} ${ ResourceManager.getString(R.string.label_currency)}")
    }
}
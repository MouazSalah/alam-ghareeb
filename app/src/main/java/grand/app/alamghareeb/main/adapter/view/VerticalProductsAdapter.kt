package grand.app.alamghareeb.main.adapter.view

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.databinding.RawProductVerticalBinding
import grand.app.alamghareeb.main.adapter.viewmodel.ItemProductViewModel
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class VerticalProductsAdapter : RecyclerView.Adapter<VerticalProductsAdapter.ProductsHolder>() {
    var itemsList: ArrayList<ProductItem> = ArrayList()
    var favLiveData = SingleLiveEvent<ProductItem>()
    var cartLiveData = SingleLiveEvent<ProductItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawProductVerticalBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_product_vertical, parent, false)
        return ProductsHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductsHolder, position: Int) {
        val itemViewModel = ItemProductViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setFav()

        holder.binding.ibFav.setOnClickListener {
            favLiveData.value = itemViewModel.item
        }

        holder.binding.rawLayout.setOnClickListener {
            cartLiveData.value = itemViewModel.item
        }

        holder.binding.btnAdd.setOnClickListener {
            cartLiveData.value = itemViewModel.item
        }

        when {
            itemViewModel.item.priceBefore != null || itemViewModel.item.priceBefore != 0 -> {
                holder.binding.tvDiscountPrice.paintFlags = holder.binding.tvDiscountPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
        }
    }

    fun getItem(pos: Int): ProductItem {
        return itemsList[pos]
    }

    fun notifyItemSelected(item: ProductItem, favState : Boolean) {
        getItem(itemsList.indexOf(item)).isFavorite = favState
        notifyItemChanged(itemsList.indexOf(item))
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ProductItem>) {
        Timber.e("mou3az_adapter : products " + models.size)
        itemsList = models
        notifyDataSetChanged()
    }

    fun removeItem(item: ProductItem) {
        itemsList.remove(item)
        notifyDataSetChanged()
    }

    inner class ProductsHolder(val binding: RawProductVerticalBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setFav(){
            when (getItem(adapterPosition).isFavorite) {
                true -> {
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_fav).into(binding.ibFav)
                }
                else -> {
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_un_fav).into(binding.ibFav)
                }
            }
        }
    }
}

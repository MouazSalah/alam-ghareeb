package grand.app.alamghareeb.main.home.viewmodel

import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.adapter.view.CategoriesAdapter
import grand.app.alamghareeb.main.adapter.view.HorizentalProductsAdapter
import grand.app.alamghareeb.main.adapter.view.SlidersAdapter
import grand.app.alamghareeb.main.favorites.model.AddToFavRequest
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.help.view.ItemHelpModel
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.home.model.HomeResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.home.view.GroupsAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import kotlin.collections.ArrayList

class HomeViewModel : BaseViewModel()
{
    var slidersAdapter = SlidersAdapter()
    var groupsAdapter = GroupsAdapter()

    var categoriesAdapter = CategoriesAdapter()
    var offersAdapter = HorizentalProductsAdapter()

    var bannersAdapter = SlidersAdapter()
    var topRatedAdapter = HorizentalProductsAdapter()

    var obsSearchNam = ObservableField<String>()

    fun onSearchClicked() {
        setValue(Codes.SEARCH_CLICKED)
    }

    fun getHomePage() {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.HOME)}) { res ->
            val response : HomeResponse = Gson().fromJson(res, HomeResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    offersAdapter.updateList(response.homeData?.offersList as ArrayList<ProductItem>)
                    topRatedAdapter.updateList(response.homeData.topRatedList as ArrayList<ProductItem>)
                    categoriesAdapter.updateList(response.homeData.categoriesList as ArrayList<CategoriesItem>)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun addToFav(item : ProductItem) {
        val request = AddToFavRequest()
        request.product_id = item.id
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.FAVORITES, request)}) { res ->
            obsIsProgress.set(false)
            val response : AddToFavResponse = Gson().fromJson(res, AddToFavResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    init {
        getHomePage()

        val list = arrayListOf(
            ItemHelpModel(0, getString(R.string.title_previos_orders), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_shopping_cart)),
            ItemHelpModel(1, getString(R.string.title_saving_offers), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_offers)),
            ItemHelpModel(2, getString(R.string.title_most_selling), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable. ic_most_selling))
        )

        groupsAdapter.updateList(list)
    }

    fun onShowAllClicked(value : Int) {
        apiResponseLiveData.value = ApiResponse.success(value)
        setValue(value)
    }

    fun onSpecialOrderClicked() {
        setValue(Codes.SHOW_SPECIAL_ORDER_DIALOG)
    }
}
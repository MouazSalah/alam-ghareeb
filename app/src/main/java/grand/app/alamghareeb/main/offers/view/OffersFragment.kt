package grand.app.alamghareeb.main.offers.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.BaseHomeFragment
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentOffersBinding
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.home.model.SliderItem
import grand.app.alamghareeb.main.offers.viewmodel.OffersViewModel
import grand.app.alamghareeb.main.offers.response.OffersResponse
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class OffersFragment : BaseHomeFragment() {

    private lateinit var binding : FragmentOffersBinding
    @Inject lateinit var viewModel: OffersViewModel
    lateinit var sliderView : SliderView
    var productItem = ProductItem()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offers, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getOffers()
        }


        binding.etSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.onSearchClicked()
                return@OnEditorActionListener true
            }
            false
        })

        observe(viewModel.offersAdapter.favLiveData){
            it?.let { item ->
                when {
                    PrefMethods.getUserData() == null -> {
                        Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                    }
                    else -> {
                        productItem = item
                        viewModel.addToFav(item)
                    }
                }
            }
        }

        observe(viewModel.offersAdapter.cartLiveData){

            it?.let { item ->
                findNavController().navigate(OffersFragmentDirections.offersToProductDetails(item.id!!))
            }
        }

        observe(viewModel.slidersAdapter.itemLiveData){
            it?.let { item ->
                findNavController().navigate(OffersFragmentDirections.offersToAllProducts(item.categoryId!!))
            }
        }

        observe(viewModel.apiResponseLiveData) { it ->
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is OffersResponse -> {
                            setupSlider(it.data.offersData?.sliderList as ArrayList<SliderItem>)
                        }
                        is AddToFavResponse -> {
                            showToast(it.data.message.toString(), 2)
                            it.data.data?.isFavorite?.let { it1 ->
                                viewModel.offersAdapter.notifyItemSelected(productItem,
                                    it1)
                            }
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.SEARCH_CLICKED -> {
                                    findNavController().navigate(R.id.offers_to_search)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 && PrefMethods.getUserData() != null -> {
                Const.isAskedToLogin = 0
                viewModel.getOffers()
            }
        }
    }

    private fun setupSlider(covers: ArrayList<SliderItem>) {
        viewModel.slidersAdapter.updateList(covers)
        sliderView = binding.sliderOffers
        sliderView.setSliderAdapter(viewModel.slidersAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }
}
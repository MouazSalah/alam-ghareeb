package grand.app.alamghareeb.main.adapter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.SliderViewAdapter
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.databinding.RawProductSliderBinding
import grand.app.alamghareeb.main.adapter.viewmodel.ItemProductSliderViewModel
import grand.app.alamghareeb.main.productdetails.model.ProductSliderItem
import grand.app.alamghareeb.utils.SingleLiveEvent

class ProductSlidersAdapter : SliderViewAdapter<ProductSlidersAdapter.ProductSliderHolder>()
{
    var itemsList =  ArrayList<ProductSliderItem>()
    var itemLiveData = SingleLiveEvent<ProductSliderItem>()

    override fun onCreateViewHolder(parent: ViewGroup?): ProductSlidersAdapter.ProductSliderHolder {
        val context = parent!!.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawProductSliderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_product_slider, parent, false)
        return ProductSliderHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductSlidersAdapter.ProductSliderHolder, position: Int) {
        val itemViewModel = ItemProductSliderViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        Glide.with(BaseApp.getInstance).load(itemViewModel.item.image).placeholder(R.drawable.ic_logo).into(holder.binding.imgSlider)

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    fun updateList(models: ArrayList<ProductSliderItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ProductSliderHolder(val binding: RawProductSliderBinding) : SliderViewAdapter.ViewHolder(binding.root)

    override fun getCount(): Int {
        return itemsList.size
    }
}

package grand.app.alamghareeb.main.orderdetails.model

import com.google.gson.annotations.SerializedName

data class GetRestoreOrderResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Any? = null,

	@field:SerializedName("message")
	val message: String? = null
)

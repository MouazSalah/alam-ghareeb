package grand.app.alamghareeb.main.filter.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.filter.model.FilterBrandItem
import grand.app.alamghareeb.main.filter.model.FilterDetailsResponse
import grand.app.alamghareeb.main.filter.model.FilterRequest
import grand.app.alamghareeb.main.filter.view.FilterBrandAdapter
import grand.app.alamghareeb.main.filter.view.FilterSubCategoryAdapter
import grand.app.alamghareeb.main.productdetails.model.ProductColorItem
import grand.app.alamghareeb.main.productdetails.view.ProductColorsAdapter
import grand.app.alamghareeb.main.products.model.SubCategoriesItem
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.requestCall
import timber.log.Timber
import java.util.ArrayList

class FilterViewModel : BaseViewModel()
{
    var filterRequest = FilterRequest()

    var subAdapter = FilterSubCategoryAdapter()
    var brandsAdapter = FilterBrandAdapter()
    var colorsAdapter = ProductColorsAdapter()

    var obsMinPrice = ObservableField<Int>()
    var obsMaxPrice = ObservableField<Int>()
    var obsShowColorLayout = ObservableField<Boolean>()
    var isPriceChanged: Int = 0
    var subCatId: Int? = null
    var brandsList: ArrayList<String>? = null

    fun onBackClicked() {
        setValue(Codes.BACK_PRESSED)
    }

    fun onConfirmClicked() {
        when {
            Params.sub_category_ids.isNullOrEmpty() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_sub_category))
            }
            Params.brands.isNullOrEmpty() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_brand))
            }
            else -> {
                setValue(Codes.SEARCH_CLICKED)
            }
        }
    }

    fun getFilterDetails() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({
            getApiRepo().getFilterDetails(URLS.FILTER_DETAILS,
                Params.categoryid!!)
        }) { res ->
            val response: FilterDetailsResponse =
                Gson().fromJson(res, FilterDetailsResponse::class.java)
            when (response.code) {
                200 -> {
                    obsMinPrice.set(response.filterDetailsData?.minPrice!!)
                    obsMaxPrice.set(response.filterDetailsData.maxPrice)

                    obsLayout.set(LoadingStatus.FULL)
                    Timber.e("mou3aaz : subCategories " + response.filterDetailsData.subCategoriesList!!.size)
                    Timber.e("mou3aaz : brands " + response.filterDetailsData.brandsList!!.size)
                    subAdapter.updateList(response.filterDetailsData.subCategoriesList as ArrayList<SubCategoriesItem>)

                    val brandsList = arrayListOf<FilterBrandItem>()
                    response.filterDetailsData.brandsList.forEach {
                        val item = FilterBrandItem(it)
                        brandsList.add(item)
                    }

                    brandsAdapter.updateList(brandsList)

                    when {
                        response.filterDetailsData.colorsList!!.size == 0 -> {
                            obsShowColorLayout.set(false)
                        }
                        else -> {
                            obsShowColorLayout.set(true)
                            colorsAdapter.updateList(response.filterDetailsData.colorsList as ArrayList<ProductColorItem>)
                        }
                    }

                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}
package grand.app.alamghareeb.main.addresses.model

import com.google.gson.annotations.SerializedName

data class ListAddressesResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val listAddressesData: ListAddressesData? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class ListAddressItem(

	@field:SerializedName("delivery_cost")
	val deliveryCost: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("street")
	val street: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("is_default")
	var isDefault: Int? = null
)

data class ListAddressesData(

	@field:SerializedName("addresses")
	val addressesList: List<ListAddressItem?>? = null,

	@field:SerializedName("free_delivery")
	val freeDelivery: Double? = null
)

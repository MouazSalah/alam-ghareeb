package grand.app.alamghareeb.main.cart.adapter

import androidx.databinding.ObservableField
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.cart.model.CartItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.resources.ResourceManager

class ItemCartViewModel(var item: CartItem) : BaseViewModel()
{
    var obsPrice = ObservableField<String>()
    var obsItemName = ObservableField<String>()
    var increaseCountLiveData = SingleLiveEvent<CartItem>()
    var decreaseCountLiveData = SingleLiveEvent<CartItem>()
    var removeItemLiveData = SingleLiveEvent<CartItem>()
    var errorLiveData = SingleLiveEvent<String>()
    var name = StringBuilder()
    val obsCount = ObservableField<Int>()

    init {
        obsCount.set(item.quantity)

        obsPrice.set("${item.price} ${ ResourceManager.getString(R.string.label_currency)}")
        name.append(item.name)
        when {
            item.color != null && item.color != "" -> {
                name.append(" - " + item.color)
            }
        }
        when {
            item.size != null && item.size != "" -> {
                name.append(" - " + item.size)
            }
        }
        obsItemName.set(name.toString())
    }

    /*
    * Tis method called when click on item cart PLUS icon
    */

    fun onPlusClicked()
    {
        when {
            obsCount.get()!!.toInt() != item.stockQuantity -> {
                obsCount.set(obsCount.get()!! + 1)
                increaseCountLiveData.value = item
            }
            else -> {
               errorLiveData.value = getString(R.string.msg_out_of_stock)
            }
        }
    }

    /*
   * Tis method called when click on item cart MINUS icon
   */
    fun onMinusClicked() {
        when {
            obsCount.get()!!.toInt() != 1 -> {
                obsCount.set(obsCount.get()!! - 1)
                decreaseCountLiveData.value = item
            }
            else -> {
                removeItemLiveData.value = item
            }
        }
    }

    fun onRemoveClicked() {
        removeItemLiveData.value = item
    }

}
package grand.app.alamghareeb.main.cart.model

import com.google.gson.annotations.SerializedName

data class BranchesResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val branchesList: List<BranchItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class BranchItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	var isSelected : Boolean = false
)

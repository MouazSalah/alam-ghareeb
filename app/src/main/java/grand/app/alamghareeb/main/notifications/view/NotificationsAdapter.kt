package grand.app.alamghareeb.main.notifications.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.databinding.RawNotificationBinding
import grand.app.alamghareeb.main.notifications.model.NotificationItem
import grand.app.alamghareeb.main.notifications.viewmodel.ItemNotificationViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class NotificationsAdapter : RecyclerView.Adapter<NotificationsAdapter.NotificationsHolder>()
{
    var itemsList: ArrayList<NotificationItem> = ArrayList()
    var itemLiveData  = SingleLiveEvent<NotificationItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationsHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawNotificationBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_notification, parent, false)
        return NotificationsHolder(binding)
    }

    override fun onBindViewHolder(holder: NotificationsHolder, position: Int) {
        val itemViewModel = ItemNotificationViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<NotificationItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class NotificationsHolder(val binding: RawNotificationBinding) : RecyclerView.ViewHolder(binding.root)
}

package grand.app.alamghareeb.main.orderdetails.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.orderdetails.model.CancelOrderResponse
import grand.app.alamghareeb.main.orderdetails.model.OrderDetailsData
import grand.app.alamghareeb.main.orderdetails.model.OrderDetailsResponse
import grand.app.alamghareeb.main.orders.model.HistoryItem
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import grand.app.alamghareeb.main.orders.view.MyOrderProductAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall

class OrderDetailsViewModel : BaseViewModel()
{
    var orderDetails = OrderDetailsData()
    var adapter = MyOrderProductAdapter()
    var obsProductsPrice = ObservableField<String>()
    var obsDeliveryCost = ObservableField<String>()
    var obsTotalPrice = ObservableField<String>()
    var obsStatus = ObservableField<String>()
    var obsPaymentMethod = ObservableField<String>()
    var obsDeliveryWay = ObservableField<String>()
    var obsShowStatusBtn = ObservableField<Boolean>()
    var obsShowCancelStatus = ObservableField<Boolean>()
    var orderStatus: Int = 1

    fun getOrderDetails(orderId : Int) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.ORDER_DETAILS + orderId) }) { res ->
            val response : OrderDetailsResponse = Gson().fromJson(res, OrderDetailsResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    orderDetails = response.orderDetailsData!!
                    adapter.updateList(response.orderDetailsData.productsList as ArrayList<OrderProductItem>)

                    obsProductsPrice.set("${response.orderDetailsData.subtotal} ${getString(R.string.label_currency)}")
                    obsDeliveryCost.set("${response.orderDetailsData.deliveryFees} ${getString(R.string.label_currency)}")
                    obsTotalPrice.set("${response.orderDetailsData.total} ${getString(R.string.label_currency)}")

                    when (response.orderDetailsData.paymentMethod) {
                        "cash" -> {
                            when {
                                PrefMethods.getLanguage() == "ar" -> {
                                    obsPaymentMethod.set("كاش")
                                }
                                else -> {
                                    obsPaymentMethod.set("Cash")
                                }
                            }
                        }
                        else -> {
                            when {
                                PrefMethods.getLanguage() == "ar" -> {
                                    obsPaymentMethod.set("أونلاين")
                                }
                                else -> {
                                    obsPaymentMethod.set("Online")
                                }
                            }
                        }
                    }

                    when (response.orderDetailsData.payType) {
                        "home" -> {
                            when {
                                PrefMethods.getLanguage() == "ar" -> {
                                    obsDeliveryWay.set("توصيل الي المنزل")
                                }
                                else -> {
                                    obsDeliveryWay.set("Home delivery")
                                }
                            }
                        }
                        else -> {
                            when {
                                PrefMethods.getLanguage() == "ar" -> {
                                    obsDeliveryWay.set("استلام من فرع " + orderDetails.branchName)
                                }
                                else -> {
                                    obsDeliveryWay.set("Receipt from the branch " + orderDetails.branchName)
                                }
                            }
                        }
                    }

                    checkStatus(response.orderDetailsData.canceled!!,
                        response.orderDetailsData.historyList)

                    notifyChange()
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    private fun checkStatus(cancelled : Int , historyList: List<HistoryItem?>?) {

        when (cancelled) {
            1 -> {
                obsShowStatusBtn.set(false)
                obsShowCancelStatus.set(true)
            }
            else -> {
                obsShowCancelStatus.set(false)
                when (historyList?.size) {
                    1 -> {
                        obsStatus.set(getString(R.string.label_cancel_order))
                        obsShowStatusBtn.set(true)
                        orderStatus = 1
                    }
                    2 -> {
                        obsStatus.set(getString(R.string.label_cancel_order))
                        obsShowStatusBtn.set(true)
                        orderStatus = 2
                    }
                    3 -> {
                        obsShowStatusBtn.set(false)
                        orderStatus = 3
                    }
                    4 -> {
                        orderStatus = 4
                        obsShowStatusBtn.set(true)
                        obsStatus.set(getString(R.string.label_restore_order))
                    }
                }
            }
        }
    }

    fun onStatusClicked() {
        when (orderStatus) {
            1 -> {
                apiResponseLiveData.value = ApiResponse.success(Codes.CANCEL_CLICKED)
            }
            2 -> {
                apiResponseLiveData.value = ApiResponse.success(Codes.CANCEL_CLICKED)
            }
            4 -> {
                apiResponseLiveData.value = ApiResponse.success(Codes.RESTORE_CLICKED)
            }
        }
    }

    fun cancelOrder(orderId : Int) {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.CANCEL_ORDER + orderId + "/cancel") }) { res ->
            obsIsProgress.set(false)
            val response : CancelOrderResponse = Gson().fromJson(res, CancelOrderResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}
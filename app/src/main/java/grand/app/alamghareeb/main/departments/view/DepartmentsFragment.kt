package grand.app.alamghareeb.main.departments.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.firebase.messaging.FirebaseMessaging
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentHomeBinding
import grand.app.alamghareeb.location.util.AddressItem
import grand.app.alamghareeb.main.home.model.HomeResponse
import grand.app.alamghareeb.main.home.model.SliderItem
import grand.app.alamghareeb.main.home.view.HomeFragmentDirections
import grand.app.alamghareeb.main.home.viewmodel.HomeViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class DepartmentsFragment : BaseFragment(), Observer<Any?>
{
    lateinit var binding: FragmentHomeBinding
    lateinit var viewModel: HomeViewModel
    lateinit var sliderView : SliderView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        when {
            PrefMethods.getUserLocation() == null -> {
//                val intent = Intent(requireActivity(), MapsActivity::class.java)
//                startActivityForResult(intent, Codes.GET_LOCATION_FROM_MAP)
            }
        }

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getHomePage()
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task->
            if (!task.isSuccessful) {
                Timber.tag("fcm_token").e(task.exception)
                return@addOnCompleteListener
            }
            if (task.result != null) {
                if (PrefMethods.getUserData() != null)
                {
                   // viewModel.updateToken(task.result!!)
                    Timber.tag("fcm_token").e(task.result!!)
                }
            }
        }

//        binding.inputHomeSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
//            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                viewModel.onSearchClicked()
//                return@OnEditorActionListener true
//            }
//            false
//        })

//        observe(viewModel.bannersAdapter.itemLiveData) { bannerItem ->
//            bannerItem?.let { item ->
//            }
//        }
//
//        observe(viewModel.categoriesAdapter.itemLiveData) { categoryItem ->
//            categoryItem?.let { item ->
//            }
//        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is HomeResponse -> {
                           // setupSlider(it.data.homeData!!.homeSlidersList as ArrayList<HomeSliderItem>)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.getHomePage()
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it)
        {
            Codes.SEARCH_CLICKED -> {
                // findNavController().navigate(R.id.home_to_search)
            }
            Codes.EMPTY_SALON_NAME -> {
                showToast(getString(R.string.msg_empty_salon_name), 1)
            }
        }
    }

    private fun setupSlider(covers: ArrayList<SliderItem>) {
       // viewModel.bannersAdapter.updateList(covers)
        sliderView = requireActivity().findViewById(R.id.slider_home_banner)
      //  sliderView.setSliderAdapter(viewModel.bannersAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /* When user select his location manually from map activity*/
        when {
            requestCode == Codes.GET_LOCATION_FROM_MAP && data != null -> {
                when {
                    data.hasExtra(Params.ADDRESS_ITEM) -> {
                        val locationItem = data.getParcelableExtra<AddressItem>(Params.ADDRESS_ITEM)
                        when {
                            locationItem != null -> {
                                PrefMethods.saveUserLocation(locationItem)
                                viewModel.getHomePage()
                            }
                        }
                    }
                }
            }
        }
    }
}
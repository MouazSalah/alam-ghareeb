package grand.app.alamghareeb.main.confirmorder.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.addresses.model.ListAddressItem

class ItemOrderAddressViewModel(var item: ListAddressItem) : BaseViewModel()
package grand.app.alamghareeb.main.home.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentHomeBinding
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.dialogs.specialorder.view.DialogSpecialOrderFragment
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.home.model.HomeResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.home.model.SliderItem
import grand.app.alamghareeb.main.home.viewmodel.HomeViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe

class HomeFragment : BaseFragment() {

    lateinit var binding: FragmentHomeBinding
    lateinit var viewModel: HomeViewModel
    lateinit var sliderView: SliderView
    lateinit var bannerView: SliderView
    var productItem = ProductItem()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.getHomePage()

        viewModel.obsSearchNam.set(null)

        observe(viewModel.mutableLiveData)
        {
            when (it) {
                Codes.SHOW_SPECIAL_ORDER_DIALOG -> {
                    when {
                        PrefMethods.getUserData() == null -> {
                            Utils.startDialogActivity(requireActivity(),
                                DialogLoginFragment::class.java.name,
                                Codes.DIALOG_LOGIN_REQUEST,
                                null)
                        }
                        else -> {
                            Utils.startDialogActivity(requireActivity(),
                                DialogSpecialOrderFragment::class.java.name,
                                Codes.DIALOG_SPECIAL_ORDER,
                                null)
                        }
                    }
                }
                Codes.SEARCH_CLICKED -> {
                    findNavController().navigate(R.id.home_to_search)
                }
                Codes.EMPTY_SALON_NAME -> {
                    showToast(getString(R.string.msg_empty_salon_name), 1)
                }
                Codes.SHOW_ALL_CATEGORIES -> {
                    findNavController().navigate(R.id.navigation_categories)
                }
                Codes.SHOW_BEST_OFFERS -> {
                    findNavController().navigate(HomeFragmentDirections.homeToTopRated(URLS.BEST_OFFERS))
                }
                Codes.SHOW_TOP_RATED -> {
                    findNavController().navigate(HomeFragmentDirections.homeToTopRated(URLS.TOP_RATED))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is HomeResponse -> {
                            setupSlider(it.data.homeData!!.slidersList as ArrayList<SliderItem>)
                            setupBanner(it.data.homeData.bannerList as ArrayList<SliderItem>)
                        }
                        is AddToFavResponse -> {
                            showToast(it.data.message.toString(), 2)
                            it.data.data?.isFavorite?.let { it1 -> viewModel.offersAdapter.notifyItemSelected(productItem, it1) }
                        }
                    }
                }
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getHomePage()
        }

        binding.etSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    viewModel.onSearchClicked()
                    return@OnEditorActionListener true
                }
                else -> false
            }
        })

        observe(viewModel.groupsAdapter.itemLiveData) { sliderItem ->
            sliderItem?.let { item ->
                findNavController().navigate(HomeFragmentDirections.homeToGroups(item.id!!))
            }
        }

        observe(viewModel.categoriesAdapter.itemLiveData) { categoryItem ->
            categoryItem?.let { item ->
                findNavController().navigate(HomeFragmentDirections.homeToProducts(item.id!!))
            }
        }

        observe(viewModel.slidersAdapter.itemLiveData) { item ->
            findNavController().navigate(HomeFragmentDirections.homeToProducts(item?.categoryId!!))
        }

        observe(viewModel.bannersAdapter.itemLiveData) { sliderItem ->
            sliderItem?.let { item ->
                // navigate to all categories
                findNavController().navigate(HomeFragmentDirections.homeToProducts(item.categoryId!!))
            }
        }

        observe(viewModel.offersAdapter.cartLiveData) { productItem ->
            productItem?.let { item ->
                findNavController().navigate(HomeFragmentDirections.homeToProductDetails(item.id!!))
            }
        }

        observe(viewModel.offersAdapter.favLiveData) { item ->
            item?.let { it ->
                when {
                    PrefMethods.getUserData() == null -> {
                        Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                    }
                    else -> {
                        productItem = it
                        viewModel.addToFav(it)
                    }
                }
            }
        }

        observe(viewModel.topRatedAdapter.cartLiveData) { productItem ->
            productItem?.let { item ->
                findNavController().navigate(HomeFragmentDirections.homeToProductDetails(item.id!!))
            }
        }

        observe(viewModel.topRatedAdapter.favLiveData) { item ->
            item?.let { it ->
                when {
                    PrefMethods.getUserData() == null -> {
                        Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                    }
                    else -> {
                        productItem = it
                        viewModel.addToFav(it)
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.getHomePage()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_SPECIAL_ORDER -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        showToast(getString(R.string.msg_order_booked), 2)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setupSlider(covers: ArrayList<SliderItem>) {
        viewModel.slidersAdapter.updateList(covers)
        sliderView = binding.homeSlider
        sliderView.setSliderAdapter(viewModel.slidersAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }


    private fun setupBanner(covers: ArrayList<SliderItem>) {
        viewModel.bannersAdapter.updateList(covers)
        bannerView = binding.homeBanners
        bannerView.setSliderAdapter(viewModel.bannersAdapter)
        bannerView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        bannerView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        bannerView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        bannerView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        bannerView.indicatorUnselectedColor = Color.WHITE
        bannerView.scrollTimeInSec = 3
        bannerView.isAutoCycle = true
        bannerView.startAutoCycle()
    }


    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 && PrefMethods.getUserData() != null -> {
                Const.isAskedToLogin = 0
                viewModel.getHomePage()
            }
        }
    }
}
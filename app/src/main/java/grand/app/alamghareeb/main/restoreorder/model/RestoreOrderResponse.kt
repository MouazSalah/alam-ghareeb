package grand.app.alamghareeb.main.restoreorder.model

import com.google.gson.annotations.SerializedName

data class RestoreOrderResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Any? = null,

	@field:SerializedName("message")
	val message: String? = null
)

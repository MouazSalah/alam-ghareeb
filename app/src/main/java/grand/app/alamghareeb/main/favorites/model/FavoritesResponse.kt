package grand.app.alamghareeb.main.favorites.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.ProductItem

data class FavoritesResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val favoritesList: List<ProductItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)
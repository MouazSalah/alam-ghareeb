package grand.app.alamghareeb.main.addaddress.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.location.location.addaddress.AddAddressRequest
import grand.app.alamghareeb.main.addaddress.model.AddAddressResponse
import grand.app.alamghareeb.main.addresses.model.*
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import timber.log.Timber
import javax.inject.Inject

open class AddAddressViewModel @Inject constructor() : BaseViewModel()
{
    var request = AddAddressRequest()
    var citiesResponse = CitiesResponse()
    var areasresponse = AreasResponse()

    fun onAddAddressClicked() {
        setClickable()
        when {
            request.name == null || request.name == ""-> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_address_name))
            }
            request.city_id == null-> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_city))
            }
            request.area_id == null-> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_area))
            }
            request.street == null || request.street == ""-> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_street_name))
            }
            request.building_no == null || request.building_no == "" -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_building_num))
            }
            request.floor == null || request.floor == "" -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_floor))
            }
            request.special_marque == null || request.special_marque == "" -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_mark))
            }
            request.phone == null || request.phone == ""-> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_phone))
            }
            else -> {
                addAddressToApi()
            }
        }
    }

    private fun addAddressToApi() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.ADD_ADDRESS, request) }) { res ->
            val response : AddAddressResponse = Gson().fromJson(res, AddAddressResponse::class.java)
            obsIsProgress.set(false)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    private fun getAllCities() {
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.ALL_CITIES) }) { res ->
            val response : CitiesResponse = Gson().fromJson(res, CitiesResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    citiesResponse = response
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun getAllAreas(cityId : Int?) {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestGetQuery(URLS.ALL_AREAS, cityId!!) }) { res ->
                val response : AreasResponse = Gson().fromJson(res, AreasResponse::class.java)
                  obsIsProgress.set(false)
            when (response.code) {
                    200 -> {
                        areasresponse = response
                        apiResponseLiveData.value = ApiResponse.success(response)
                    }
                    else -> {
                        apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                    }
                }
            }
    }

    fun onCityClicked()
    {
        setValue(Codes.CITY_CLICKED)
    }

    fun onAreaClicked()
    {
        when (request.city_id) {
            null -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_select_city_first))
            }
            else -> {
                setValue(Codes.AREA_CLICKED)
            }
        }
    }

    init {
        getAllCities()
    }
}
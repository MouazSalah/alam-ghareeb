package grand.app.alamghareeb.main.productdetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawSizeBinding
import grand.app.alamghareeb.main.productdetails.model.ProductSizeItem
import grand.app.alamghareeb.main.productdetails.viewmodel.ItemSizeViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class ProductAttributesAdapter : RecyclerView.Adapter<ProductAttributesAdapter.ProductAttributeHolder>()
{
    var itemsList: ArrayList<ProductSizeItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductAttributeHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawSizeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_size, parent, false)
        return ProductAttributeHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductAttributeHolder, position: Int) {
        val itemViewModel = ItemSizeViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ProductSizeItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ProductAttributeHolder(val binding: RawSizeBinding) : RecyclerView.ViewHolder(binding.root)
}

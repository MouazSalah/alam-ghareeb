package grand.app.alamghareeb.main.profile.viewmodel

import android.graphics.drawable.Drawable

data class ItemProfileViewModel(
        var id : Int ?= null,
        var title : String ?= null,
        var image : Drawable?= null
)

package grand.app.alamghareeb.main.addresses.model

data class SetDefaultAddressRequest(
	val address_id: Int? = null
)

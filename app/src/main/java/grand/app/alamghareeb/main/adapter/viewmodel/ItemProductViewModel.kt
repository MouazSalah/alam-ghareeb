package grand.app.alamghareeb.main.adapter.viewmodel

import androidx.databinding.ObservableField
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.home.model.ProductItem
import timber.log.Timber

class ItemProductViewModel(var item: ProductItem) : BaseViewModel()
{
    var obsPrice = ObservableField<String>()
    var obsRate : Float = 0f
    var obsPriceBefore = ObservableField<String>()

    init {
        obsRate = when {
            item.rate != null -> {
                item.rate as Float
            }
            else -> {
                0f
            }
        }

        Timber.e("mou3az_price : " + item.priceBefore)

        when {
            item.priceBefore != null && item.priceBefore != 0 -> {
                obsIsVisible.set(true)
                obsPriceBefore.set("${item.priceBefore} ${getString(R.string.label_product_currency)}")
            }
            else -> {
                obsIsVisible.set(false)
            }
        }
        obsPrice.set("${item.price} ${getString(R.string.label_product_currency)}")
    }
}
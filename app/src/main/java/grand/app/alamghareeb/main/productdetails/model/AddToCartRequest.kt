package grand.app.alamghareeb.main.productdetails.model

data class AddToCartRequest(
    var product_id: Int? = null,
    var quantity: Int? = null,
    var size: Int? = null,
    var color: Int? = null,
    var attribute_parent_id: Int? = null,
    var attribute_child_id: Int? = null
)

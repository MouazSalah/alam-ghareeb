package grand.app.alamghareeb.main.orders.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawMyOrderProductBinding
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import grand.app.alamghareeb.main.orders.viewmodel.ItemOrderProductViewModel
import kotlin.collections.ArrayList

class MyOrderProductAdapter : RecyclerView.Adapter<MyOrderProductAdapter.CartsHolder>()
{
    var itemsList: ArrayList<OrderProductItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartsHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawMyOrderProductBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_my_order_product, parent, false)
        return CartsHolder(binding)
    }

    override fun onBindViewHolder(holder: CartsHolder, position: Int) {
        val itemViewModel = ItemOrderProductViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: java.util.ArrayList<OrderProductItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CartsHolder(val binding: RawMyOrderProductBinding) : RecyclerView.ViewHolder(binding.root)
}

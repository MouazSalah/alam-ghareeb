package grand.app.alamghareeb.main.cart.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawCartBinding
import grand.app.alamghareeb.main.cart.adapter.ItemCartViewModel
import grand.app.alamghareeb.main.cart.model.CartItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class CartAdapter : RecyclerView.Adapter<CartAdapter.CartsHolder>()
{
    var itemsList: ArrayList<CartItem> = ArrayList()
    var removeCartLiveData  = SingleLiveEvent<CartItem>()
    var favLiveData  = SingleLiveEvent<CartItem>()
    var increaseLiveData = SingleLiveEvent<CartItem>()
    var decreaseLiveData = SingleLiveEvent<CartItem>()
    var outOfStock = SingleLiveEvent<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartsHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawCartBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_cart, parent, false)
        return CartsHolder(binding)
    }

    override fun onBindViewHolder(holder: CartsHolder, position: Int) {
        val itemViewModel = ItemCartViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.ibFav.setOnClickListener {
            favLiveData.value = itemViewModel.item
        }

        itemViewModel.increaseCountLiveData.observeForever {
            increaseLiveData.value = it
        }

        itemViewModel.errorLiveData.observeForever {
            outOfStock.value = it
        }

        itemViewModel.decreaseCountLiveData.observeForever {
            decreaseLiveData.value = it
        }

        itemViewModel.removeItemLiveData.observeForever {
            removeCartLiveData.value = it
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun notifyItemSwiped(pos : Int) {

    }

    fun updateList(models: ArrayList<CartItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CartsHolder(val binding: RawCartBinding) : RecyclerView.ViewHolder(binding.root)
}

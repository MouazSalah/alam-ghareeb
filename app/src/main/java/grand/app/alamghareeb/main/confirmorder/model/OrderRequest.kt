package grand.app.alamghareeb.main.confirmorder.model

data class OrderRequest (
    var pay_type: String? = null,
    var branch_id: Int? = null,

    var address: String? = null,
    var address_id: Int ?= null,

    var subtotal: Double? = null,
    var discount: Double? = null,
    var delivery_fees: Double? = null,
    var total: Double? = null,

    var is_wallet: Int ?= null,
    var wallet_amount: Double? = null,

    var description: String? = null,
    var payment_method: String? = null,

    var products: ArrayList<OrderProductRequest> = ArrayList()
)

data class OrderProductRequest (
    var product_id: Int? = null,
    var name: String? = null,
    var quantity: Int? = null,
    var price: Double ?= null,
    var products_total: Double? = null,
    var attribute_parent_id: Int? = null,
    var attribute_child_id: Int? = null,
    var attribute_parent_name: Int? = null,
    var attribute_child_name: Int? = null
)
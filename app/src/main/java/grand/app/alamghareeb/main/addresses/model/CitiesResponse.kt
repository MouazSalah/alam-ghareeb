package grand.app.alamghareeb.main.addresses.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CitiesResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val citiesList: List<CityItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Serializable

data class CityItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
) : Serializable
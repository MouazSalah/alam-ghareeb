package grand.app.alamghareeb.main.adapter.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.productdetails.model.ProductSliderItem

class ItemProductSliderViewModel(var item: ProductSliderItem) : BaseViewModel()
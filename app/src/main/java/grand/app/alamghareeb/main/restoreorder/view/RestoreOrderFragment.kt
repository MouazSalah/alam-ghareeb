package grand.app.alamghareeb.main.restoreorder.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentRestoreOrderBinding
import grand.app.alamghareeb.dialogs.wallet.DialogWalletFragment
import grand.app.alamghareeb.dialogs.quantity.view.DialogQuantityFragment
import grand.app.alamghareeb.dialogs.reasons.view.DialogReasonsFragment
import grand.app.alamghareeb.dialogs.restoresuccess.DialogRestoreSuccessFragment
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import grand.app.alamghareeb.main.restoreorder.model.RestoreOrderResponse
import grand.app.alamghareeb.main.restoreorder.viewmodel.RestoreOrderViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import java.util.ArrayList

class RestoreOrderFragment : BaseFragment()
{
    lateinit var binding: FragmentRestoreOrderBinding
    lateinit var viewModel: RestoreOrderViewModel
    val fragmentArgs : RestoreOrderFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_restore_order, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(RestoreOrderViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.adapter.updateList(fragmentArgs.orderDetails.productsList as ArrayList<OrderProductItem>)

        observe(viewModel.adapter.itemLiveData){ item ->
            item?.id.let { id ->
                viewModel.request.product_id = id
                viewModel.request.quantity= item?.quantity
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is RestoreOrderResponse -> {
                            Utils.startDialogActivity(requireActivity(), DialogRestoreSuccessFragment::class.java.name, Codes.DIALOG_RESTORE_SUCCESS, null)
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.CANCEL_CLICKED -> {
                                    Utils.startDialogActivity(requireActivity(), DialogWalletFragment::class.java.name, Codes.DIALOG_CONFIRM_REQUEST, null)
                                }
                                Codes.REASON_CLICKED -> {
                                    val bundle = Bundle()
                                    bundle.putSerializable(Params.REASONS_RESPONSE, viewModel.reasonsResponse)
                                    Utils.startDialogActivity(requireActivity(), DialogReasonsFragment::class.java.name, Codes.DIALOG_RESTORE_REASONS, bundle)
                                }
                                Codes.QUANTITY_CLICKED -> {
                                    val bundle = Bundle()
                                    bundle.putInt(Params.ORDER_Quantity, viewModel.request.quantity!!)
                                    Utils.startDialogActivity(requireActivity(), DialogQuantityFragment::class.java.name, Codes.DIALOG_RESTORE_QUANTITY, bundle)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_CONFIRM_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                       // viewModel.cancelOrder(fragmentArgs.orderId)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_RESTORE_QUANTITY -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.request.quantity =  data.getIntExtra(Params.ORDER_Quantity, 1)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_RESTORE_REASONS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.request.reason =  data.getStringExtra(Params.RESTORE_REASON)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
package grand.app.alamghareeb.main.products.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.BaseHomeFragment
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentProductsBinding
import grand.app.alamghareeb.dialogs.dialogsort.DialogSortFragment
import grand.app.alamghareeb.dialogs.dialogsort.SortItem
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.main.cart.model.CartItem
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.filter.model.FilterRequest
import grand.app.alamghareeb.main.filter.view.FilterActivity
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.offers.response.OffersResponse
import grand.app.alamghareeb.main.offers.view.OffersFragmentDirections
import grand.app.alamghareeb.main.products.viewmodel.ProductsViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject

class ProductsFragment : BaseHomeFragment() {

    private lateinit var binding : FragmentProductsBinding
    @Inject lateinit var viewModel: ProductsViewModel
    val fragmentArgs : ProductsFragmentArgs by navArgs()
    private var productItem = ProductItem()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_products, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        viewModel.categoryId = fragmentArgs.categoryId
        viewModel.getProductsByCategory(fragmentArgs.categoryId)

        binding.ibBack.setOnClickListener {
            findNavController().navigate(R.id.navigation_home)
        }

        binding.ibSearch.setOnClickListener {
            findNavController().navigate(R.id.products_to_search)
        }

        when {
            PrefMethods.getUserCarts()!!.isNullOrEmpty() -> {
                binding.cartLayout.visibility = View.GONE
            }
            else -> {
                binding.cartLayout.visibility = View.VISIBLE
                var totalPrice = 0.0
                val cartsList: ArrayList<CartItem> = PrefMethods.getUserCarts()!!
                for (i in cartsList) {
                    totalPrice = totalPrice.plus(i.total!!)
                }
                binding.tvCartCount.text = cartsList.size.toString()
                binding.tvCartPrice.text = "$totalPrice ${getString(R.string.label_currency)}"
            }
        }

        binding.cartLayout.setOnClickListener {
             navController.navigate(R.id.products_to_carts)
        }

        observe(viewModel.categoriesAdapter.itemLiveData){
            it?.id?.let { it1 -> viewModel.getProductsByCategory(it1) }
        }

        observe(viewModel.subCategoriesAdapter.itemLiveData){
            it?.id?.let { item ->
                when (item) {
                    0 -> {
                        viewModel.getProductsByCategory(fragmentArgs.categoryId)
                    }
                    else -> {
                        viewModel.getProductsBySubCategory(fragmentArgs.categoryId, item)
                    }
                }
            }
        }

        observe(viewModel.productsAdapter.favLiveData){
            it?.let { item ->
                when {
                    PrefMethods.getUserData() == null -> {
                        Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                    }
                    else -> {
                        productItem = item
                        viewModel.addToFav(item)
                    }
                }
            }
        }

        observe(viewModel.productsAdapter.cartLiveData){
            it?.let { item ->
                findNavController().navigate(ProductsFragmentDirections.productsToProductDetails(item.id!!))
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddToFavResponse -> {
                            showToast(it.data.message.toString(), 2)
                            it.data.data?.isFavorite?.let { it1 ->
                                viewModel.productsAdapter.notifyItemSelected(productItem,
                                    it1)
                            }
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.SEARCH_CLICKED -> {
                                    findNavController().navigate(R.id.products_to_search)
                                }
                                Codes.SORT_CLICKED -> {
                                    val bundle = Bundle()
                                    bundle.putInt(Params.SORT_FLAG, Codes.SORT_PRODUCTS)
                                    Utils.startDialogActivity(requireActivity(),
                                        DialogSortFragment::class.java.name,
                                        Codes.DIALOG_SORT_ORDERS,
                                        bundle)
                                }
                                Codes.FILTER_CLICKED -> {
                                    Params.categoryid = viewModel.categoryId
                                    requireActivity().startActivityForResult(Intent(requireActivity(),
                                        FilterActivity::class.java), Codes.FILTER_REQUEST)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_SORT_ORDERS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        var sortItem = data.getSerializableExtra(Params.SORT_ITEM) as SortItem
                                        viewModel.sortOrders(sortItem.value)
                                        Timber.e("mou3aaz : " + sortItem.toString())
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.FILTER_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        //  val filterRequest = data.getSerializableExtra(Params.FILTER_REQUEST) as FilterRequest

                                        val request = FilterRequest(min_price = Params.min_price,
                                            max_price = Params.max_price,
                                            sub_category_ids = Params.sub_category_ids,
                                            brands = Params.brands)

                                        Timber.e("mou3az_filter : " + Params.sub_category_ids.toString())
                                        Timber.e("mou3az_filter : " + Params.brands.toString())
                                        Timber.e("mou3az_filter : " + request.toString())

                                        viewModel.filterOrders(request)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 && PrefMethods.getUserData() != null -> {
                Const.isAskedToLogin = 0
                viewModel.getProductsByCategory(fragmentArgs.categoryId)
            }
        }
    }
}
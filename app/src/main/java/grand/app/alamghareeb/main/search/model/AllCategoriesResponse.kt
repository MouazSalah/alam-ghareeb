package grand.app.alamghareeb.main.search.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.CategoriesItem

data class AllCategoriesResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val allCategoriesData: AllCategoriesData? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class AllCategoriesData(

	@field:SerializedName("categories")
	val allCategoriesList: List<CategoriesItem?>? = null
)

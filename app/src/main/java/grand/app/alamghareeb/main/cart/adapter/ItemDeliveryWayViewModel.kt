package grand.app.alamghareeb.main.cart.adapter

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.cart.model.BranchItem
import grand.app.alamghareeb.main.cart.model.DeliveryWayItem

class ItemDeliveryWayViewModel(var item: DeliveryWayItem) : BaseViewModel()
{
    var adapter = BranchesAdapter()

    init {
        when (item.id) {
            1 -> {
                adapter.updateList(item.list as ArrayList<BranchItem>)
            }
        }
    }
}
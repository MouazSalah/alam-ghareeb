package grand.app.alamghareeb.main.addaddress.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentAddAddressBinding
import grand.app.alamghareeb.dialogs.areas.DialogAreasFragment
import grand.app.alamghareeb.dialogs.cities.DialogCitiesFragment
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.main.addaddress.model.AddAddressResponse
import grand.app.alamghareeb.main.addaddress.viewmodel.AddAddressViewModel
import grand.app.alamghareeb.main.addresses.model.AreaItem
import grand.app.alamghareeb.main.addresses.model.CityItem
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.LocalUtil
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class AddAddressActivity : AppCompatActivity()
{
    lateinit var binding: FragmentAddAddressBinding
    @Inject lateinit var viewModel: AddAddressViewModel

    override fun onCreate(savedInstanceState: Bundle?)
    {
        LocalUtil.changeLanguage(this)
        super.onCreate(savedInstanceState)
        LocalUtil.changeLanguage(this)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_add_address)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddAddressResponse -> {
                            val intent = Intent()
                            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                            setResult(Codes.ADD_ADDRESS_REQUEST_CODE, intent)
                            finish()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        binding.ibBack.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
            setResult(Codes.ADD_ADDRESS_REQUEST_CODE, intent)
            finish()
        }

        observe(viewModel.mutableLiveData)
        {
            when(it)
            {
                Codes.CITY_CLICKED -> {
                    when {
                        viewModel.citiesResponse.citiesList!!.isNotEmpty() -> {
                            val bundle = Bundle()
                            bundle.putSerializable(Params.CITIES_RESPONSE, viewModel.citiesResponse)
                            Utils.startDialogActivity(this, DialogCitiesFragment::class.java.name, Codes.DIALOG_SELECT_CITY, bundle)
                        }
                    }
                }
                Codes.AREA_CLICKED -> {
                    when {
                        viewModel.areasresponse.areasList!!.isNotEmpty() -> {
                            val bundle = Bundle()
                            bundle.putSerializable(Params.AREAS_RESPONSE, viewModel.areasresponse)
                            Utils.startDialogActivity(this, DialogAreasFragment::class.java.name, Codes.DIALOG_SELECT_AREA, bundle)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode)
        {
            /* When User back from Select Date and Time */
            Codes.DIALOG_SELECT_CITY -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.CITY_ITEM) -> {
                                                val cityItem =  data.getSerializableExtra(Params.CITY_ITEM) as CityItem
                                                viewModel.request.city_id = cityItem.id
                                                binding.tvCity.text = cityItem.name
                                                viewModel.getAllAreas(cityItem.id)
                                                viewModel.request.area_id = null
                                                binding.tvArea.text = getString(R.string.hint_area)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Codes.DIALOG_SELECT_AREA -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        when {
                                            data.hasExtra(Params.AREA_ITEM) -> {
                                                val areaItem =  data.getSerializableExtra(Params.AREA_ITEM) as AreaItem
                                                viewModel.request.area_id = areaItem.id
                                                binding.tvArea.text = areaItem.name
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showToast(msg : String, type : Int) {
        // success 2
        // false  1
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(this, DialogToastFragment::class.java.name, Codes.DIALOG_TOAST_REQUEST, bundle)
    }
}


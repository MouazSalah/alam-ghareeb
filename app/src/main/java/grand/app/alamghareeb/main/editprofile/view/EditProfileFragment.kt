package grand.app.alamghareeb.main.editprofile.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fxn.pix.Pix
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentEditProfileBinding
import grand.app.alamghareeb.main.editprofile.response.UpdateProfileResponse
import grand.app.alamghareeb.main.editprofile.viewmodel.EditProfileViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class EditProfileFragment : BaseFragment()
{
    lateinit var binding : FragmentEditProfileBinding
    lateinit var viewModel : EditProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding.viewModel = viewModel

        binding.layoutRegisterPass.setOnClickListener {
            findNavController().navigate(R.id.edit_profile_to_change_pass)
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.CHOOSE_IMAGE_CLICKED -> {
                    pickImage(Codes.USER_PROFILE_IMAGE_REQUEST)
                }
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is UpdateProfileResponse -> {
                            showToast(it.data.message.toString(), 2)
                            findNavController().navigateUp()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    Codes.USER_PROFILE_IMAGE_REQUEST -> {
                        data?.let {
                            val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                            returnValue?.let { array ->
                                if (requestCode == Codes.USER_PROFILE_IMAGE_REQUEST) {
                                    val returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                                    binding.imgItem.setImageURI(returnValue[0].toUri())
                                    viewModel.obsUserImg.set(returnValue[0].toUri())
                                    viewModel.gotImage(requestCode, array[0])
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}
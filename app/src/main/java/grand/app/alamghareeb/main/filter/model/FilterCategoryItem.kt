package grand.app.alamghareeb.main.filter.model

data class FilterCategoryItem (
        var type: Int? = null,
        var name: String? = null
)
package grand.app.alamghareeb.main.filter.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.cart.model.BranchItem
import grand.app.alamghareeb.main.products.model.SubCategoriesItem

class ItemFilterSubCategoryViewModel(var item: SubCategoriesItem) : BaseViewModel()

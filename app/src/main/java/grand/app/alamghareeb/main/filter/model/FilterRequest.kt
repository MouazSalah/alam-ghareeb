package grand.app.alamghareeb.main.filter.model

import java.io.Serializable

data class FilterRequest(
    var sub_category_ids: ArrayList<Int>? = null,
    var brands: ArrayList<String>? = null,
    var min_price: Int? = null,
    var max_price: Int? = null,
    var parent_id: Int? = null,
    var child_id: Int? = null
) : Serializable
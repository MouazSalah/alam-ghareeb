package grand.app.alamghareeb.main.wallet.model

import com.google.gson.annotations.SerializedName

data class WalletResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val walletData: WalletData? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class WalletProductItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("price_before")
	val priceBefore: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("is_favorite")
	val isFavorite: Boolean? = null,

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("color")
	val color: Any? = null,

	@field:SerializedName("size")
	val size: Any? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("quantity_count")
	val quantityCount: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class WalletData(

	@field:SerializedName("total_restore")
	val totalRestore: Int? = null,

	@field:SerializedName("balance")
	val balance: Int? = null,

	@field:SerializedName("products")
	val walletProductsList: List<WalletProductItem?>? = null
)

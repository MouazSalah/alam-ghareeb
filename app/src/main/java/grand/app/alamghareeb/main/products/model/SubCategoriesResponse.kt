package grand.app.alamghareeb.main.products.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.home.model.SliderItem
import grand.app.alamghareeb.main.orders.model.Links
import grand.app.alamghareeb.main.orders.model.Meta

data class SubCategoriesResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val subCategoriesData: SubCategoriesData? = null,

    @field:SerializedName("message")
    val message: String? = null
)


//data class Products(
//
//	@field:SerializedName("data")
//	val data: List<DataItem?>? = null,
//
//	@field:SerializedName("meta")
//	val meta: Meta? = null,
//
//	@field:SerializedName("links")
//	val links: Links? = null
//)

data class DataItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("price_before")
    val priceBefore: Int? = null,

    @field:SerializedName("is_favorite")
    val isFavorite: Boolean? = null,

    @field:SerializedName("rate")
    val rate: Int? = null,

    @field:SerializedName("price")
    val price: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class SubCategoryProductsItem(

    @field:SerializedName("image")
    val image: Any? = null,

    @field:SerializedName("category_id")
    val categoryId: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("products")
    val productsList: ProductsData? = null
)

data class SubCategoriesData(

    @field:SerializedName("sub_category")
    val subCategoryList: SubCategoryProductsItem? = null,

    @field:SerializedName("banner")
    val bannerList: List<SliderItem?>? = null,

    @field:SerializedName("categories")
    val categories: List<CategoriesItem?>? = null
)

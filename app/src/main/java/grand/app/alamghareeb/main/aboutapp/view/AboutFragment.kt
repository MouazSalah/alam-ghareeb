package grand.app.alamghareeb.main.aboutapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentAboutAppBinding
import grand.app.alamghareeb.main.aboutapp.viewmodel.AboutViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class AboutFragment : BaseFragment()
{
    lateinit var binding: FragmentAboutAppBinding
    @Inject
    lateinit var viewModel: AboutViewModel
    val fragmentArgs: AboutFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_app, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            checkFlag()
        }

        checkFlag()

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    private fun checkFlag()
    {
        when (fragmentArgs.flag) {
            Codes.TERMS -> {
                viewModel.getDetails(URLS.TERMS_AND_CONDITIONS)
                binding.tvTitle.text = getString(R.string.title_terms)
                binding.imgAbout.visibility = View.GONE
                binding.imgTerms.visibility = View.VISIBLE
            }
            Codes.PRIVACY_POLICY -> {
                viewModel.getDetails(URLS.PRIVACY_POLICY)
                binding.tvTitle.text = getString(R.string.title_privacy_policy)
                binding.imgAbout.visibility = View.GONE
                binding.imgTerms.visibility = View.VISIBLE
            }
            Codes.ABOUT_APP -> {
                binding.imgAbout.visibility = View.VISIBLE
                binding.imgTerms.visibility = View.GONE
                viewModel.getDetails(URLS.ABOUT_APP)
                binding.tvTitle.text = getString(R.string.title_about_app)
            }
        }
    }
}
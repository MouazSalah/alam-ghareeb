package grand.app.alamghareeb.main.restoreorder.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ReasonsResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val reasonsList: List<ReasonItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null

) : Serializable

data class ReasonItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null

): Serializable

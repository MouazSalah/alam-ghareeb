package grand.app.alamghareeb.main.wallet.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawWalletBinding
import grand.app.alamghareeb.main.wallet.model.WalletProductItem
import grand.app.alamghareeb.main.wallet.viewmodel.ItemWalletViewModel
import java.util.*

class WalletAdapter : RecyclerView.Adapter<WalletAdapter.WalletOrdersHolder>()
{
    var itemsList: ArrayList<WalletProductItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalletOrdersHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawWalletBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_wallet, parent, false)
        return WalletOrdersHolder(binding)
    }

    override fun onBindViewHolder(holder: WalletOrdersHolder, position: Int) {
        val itemViewModel = ItemWalletViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<WalletProductItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class WalletOrdersHolder(val binding: RawWalletBinding) : RecyclerView.ViewHolder(binding.root)
}

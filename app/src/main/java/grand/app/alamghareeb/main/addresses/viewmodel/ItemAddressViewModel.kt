package grand.app.alamghareeb.main.addresses.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.addresses.model.ListAddressItem

class ItemAddressViewModel(var item: ListAddressItem) : BaseViewModel()
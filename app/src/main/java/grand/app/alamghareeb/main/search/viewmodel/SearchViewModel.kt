package grand.app.alamghareeb.main.search.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.search.model.AllCategoriesResponse
import grand.app.alamghareeb.main.search.view.SearchAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall

class SearchViewModel : BaseViewModel()
{
    var adapter = SearchAdapter()
    var obsName = ObservableField<String>()
    var categoriesList = arrayListOf<CategoriesItem>()

    init {
        getCategories()
    }

    private fun getCategories()
    {
        obsLayout.set(LoadingStatus.FULL)
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.ALL_CATEGORIES)}) { res ->
            obsIsProgress.set(false)
            val response : AllCategoriesResponse = Gson().fromJson(res, AllCategoriesResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    categoriesList =
                        response.allCategoriesData?.allCategoriesList as ArrayList<CategoriesItem>
                    adapter.allItemsList = categoriesList
                    adapter.updateList(categoriesList)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}
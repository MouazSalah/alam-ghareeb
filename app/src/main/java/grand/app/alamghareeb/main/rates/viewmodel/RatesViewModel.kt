package grand.app.alamghareeb.main.rates.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.rates.response.RateItem
import grand.app.alamghareeb.main.rates.response.RatesResponse
import grand.app.alamghareeb.main.rates.view.RatesAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import java.util.ArrayList

class RatesViewModel : BaseViewModel()
{
    var adapter = RatesAdapter()

    fun getRates(id : Int) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.RATES + id + "/rates")}) { res ->
            obsIsProgress.set(false)
            val response: RatesResponse = Gson().fromJson(res, RatesResponse::class.java)
            when (response.code) {
                200 -> {
                    when {
                        response.ratesData?.ratesList!!.isNotEmpty() -> {
                            adapter.updateList(response.ratesData.ratesList as ArrayList<RateItem>)
                            obsLayout.set(LoadingStatus.FULL)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                    }
                }
                else -> {
                    obsLayout.set(LoadingStatus.EMPTY)
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onAddRateClicked()
    {
        when {
            PrefMethods.getUserData() == null -> {
                setValue(Codes.NOT_LOGIN)
            }
            else -> {
                setValue(Codes.RATE_APP_CLICKED)
            }
        }
    }
}
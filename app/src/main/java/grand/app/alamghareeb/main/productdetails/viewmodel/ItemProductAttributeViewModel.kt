package grand.app.alamghareeb.main.productdetails.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.productdetails.model.ProductSizeItem

class ItemProductAttributeViewModel(var item: ProductSizeItem) : BaseViewModel()
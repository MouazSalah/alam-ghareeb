package grand.app.alamghareeb.main.suggestion.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.suggestion.model.SuggestRequest
import grand.app.alamghareeb.main.suggestion.model.SuggestResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.isEmailValid
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class SuggestViewModel @Inject constructor() : BaseViewModel()
{
    var request = SuggestRequest()

    fun onSendClicked() {
        setClickable()
        when {
            request.name.isNullOrEmpty() || request.name.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_name))
            }
            request.email.isNullOrEmpty() || request.email.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_email))
            }
            !isEmailValid(request.email!!) -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_invalid_email))
            }
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_phone))
            }
            request.message.isNullOrEmpty() || request.message.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_message))
            }
            else -> {
                sendSuggest()
            }
        }
    }

    private fun sendSuggest() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.SEND_SUGGEST, request) }) { res ->
            obsIsProgress.set(false)
            val response: SuggestResponse = Gson().fromJson(res, SuggestResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.msg)
                }
            }
        }
    }
}

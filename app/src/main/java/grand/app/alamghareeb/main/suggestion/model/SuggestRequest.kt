package grand.app.alamghareeb.main.suggestion.model

class SuggestRequest {
    var name: String? = null
    var email: String? = null
    var phone: String? = null
    var message: String? = null
}
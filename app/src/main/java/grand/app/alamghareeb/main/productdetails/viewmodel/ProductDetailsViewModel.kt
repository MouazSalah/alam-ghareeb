package grand.app.alamghareeb.main.productdetails.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import  com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.adapter.view.ProductSlidersAdapter
import grand.app.alamghareeb.main.adapter.view.HorizentalProductsAdapter
import grand.app.alamghareeb.main.cart.model.CartItem
import grand.app.alamghareeb.main.favorites.model.AddToFavRequest
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.productdetails.model.*
import grand.app.alamghareeb.main.productdetails.view.ProductColorsAdapter
import grand.app.alamghareeb.main.productdetails.view.ProductSizesAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import grand.app.alamghareeb.utils.resources.ResourceManager
import javax.inject.Inject

open class ProductDetailsViewModel @Inject constructor() : BaseViewModel()
{
    var productDetails = ProductDetailsData()
    var sliderAdapter = ProductSlidersAdapter()
    var colorsAdapter = ProductColorsAdapter()
    var sizesAdapter = ProductSizesAdapter()
    var relatedProductsAdapter = HorizentalProductsAdapter()
    var obsIsFav = ObservableBoolean()
    var obsPrice = ObservableField<String>()
    var obsRate = ObservableField<Float>()
    var obsTotalRating = ObservableField<String>()
    var cartItem = CartItem()
    val cartsList: ArrayList<CartItem>? = PrefMethods.getUserCarts()
    var colorItem = ProductColorItem()
    var sizeItem = ProductSizeItem()
    var productSize : Double ?= null

    fun getProductDetails(productId: Int) {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.PRODUCT_DETAILS + "/" + productId) }) { res ->
            obsIsProgress.set(false)
            val response: ProductDetailsResponse =
                Gson().fromJson(res, ProductDetailsResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    productDetails = response.productDetailsData!!

                    obsIsFav.set(response.productDetailsData.isFavorite!!)
                    obsPrice.set("${response.productDetailsData.price} ${ResourceManager.getString(R.string.label_currency)}")

                    when (response.productDetailsData.rate) {
                        null -> {
                            obsRate.set(0f)
                        }
                        else -> {
                            obsRate.set(response.productDetailsData.rate.toFloat())
                        }
                    }
                    when (response.productDetailsData.rateCount) {
                        0 -> {
                            obsTotalRating.set(getString(R.string.label_no_restes_till_now))
                        }
                        null -> {
                            obsTotalRating.set(getString(R.string.label_no_restes_till_now))
                        }
                        else -> {
                            obsTotalRating.set("${response.productDetailsData.rateCount} ${
                                getString(R.string.label_rates)
                            }")
                        }
                    }
                    when {
                        response.productDetailsData.colorsList?.size != 0 -> {
                            colorsAdapter.updateList(response.productDetailsData.colorsList as ArrayList<ProductColorItem>)
                            when {
                                response.productDetailsData.colorsList[0].sizesList?.size != 0 -> {
                                    sizesAdapter.updateList(response.productDetailsData.colorsList[0].sizesList as ArrayList<ProductSizeItem>)
                                }
                            }
                        }
                        else -> {

                        }
                    }
                    when {
                        response.productDetailsData.relatedProducts?.size != 0 -> {
                            relatedProductsAdapter.updateList(response.productDetailsData.relatedProducts as ArrayList<ProductItem>)
                        }
                    }
                    notifyChange()
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onBtnClicked(value: Int) {

        when (value) {
            Codes.FAVORITE_CLICKED -> {
                addToFav(productDetails.id!!)
            }
            else -> {
                apiResponseLiveData.value = ApiResponse.success(value)
            }
        }
    }

    fun addToFav(itemId: Int) {
        val request = AddToFavRequest()
        request.product_id = itemId
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.FAVORITES, request) }) { res ->
            obsIsProgress.set(false)
            val response: AddToFavResponse = Gson().fromJson(res, AddToFavResponse::class.java)
            when (response.code) {
                200 -> {
                    obsIsFav.set(response.data?.isFavorite!!)
                    apiResponseLiveData.value = ApiResponse.successMessage(response.message)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onAddToCart() {
        cartItem.id = productDetails.id
        cartItem.name = productDetails.name
        cartItem.image = productDetails.productImagesList!![0]?.image
        cartItem.priceBefore = productDetails.priceBefore
        cartItem.price = productDetails.price
        cartItem.isFavorite = productDetails.isFavorite
        cartItem.quantity = 1
        cartItem.rate = productDetails.rate
        cartItem.total = productDetails.price
        cartItem.quantity = 1
        cartItem.stockQuantity = productDetails.quantity

        when {
            productDetails.colorsList!!.isNotEmpty() -> {
                when (colorsAdapter.selectedPosition) {
                    -1 -> {
                        apiResponseLiveData.value =
                            ApiResponse.errorMessage(getString(R.string.msg_empty_selected_color))
                    }
                    else -> {
                        cartItem.colorId = colorItem.id
                        cartItem.color = colorItem.name
                        var pos = colorsAdapter.selectedPosition
                        if (colorsAdapter.itemsList[pos].sizesList!!.isNotEmpty()) {
                            when (sizeItem.id) {
                                null -> {
                                    apiResponseLiveData.value =
                                        ApiResponse.errorMessage(getString(R.string.msg_empty_selected_size))
                                }
                                else -> {
                                    cartItem.sizeId = sizeItem.id
                                    cartItem.size = sizeItem.name
                                    addToOfflineCart(cartItem)
                                }
                            }
                        } else {
                            cartItem.sizeId = null
                            cartItem.size = null
                            addToOfflineCart(cartItem)
                        }
                    }
                }
            }
            else -> {
                addToOfflineCart(cartItem)
            }
        }

//        if (productDetails.quantity != 0)
//        {
//            when {
//                productDetails.colorsList!!.isNotEmpty() -> {
//                    when (colorsAdapter.selectedPosition) {
//                        -1 -> {
//                            apiResponseLiveData.value =
//                                ApiResponse.errorMessage(getString(R.string.msg_empty_selected_color))
//                        }
//                        else -> {
//                            cartItem.colorId = colorItem.id
//                            cartItem.color = colorItem.name
//                            var pos = colorsAdapter.selectedPosition
//                            if (colorsAdapter.itemsList[pos].sizesList!!.isNotEmpty())
//                            {
//                                when (sizeItem.id) {
//                                    null -> {
//                                        apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_empty_selected_size))
//                                    }
//                                    else -> {
//                                        cartItem.sizeId = sizeItem.id
//                                        cartItem.size = sizeItem.name
//                                        addToOfflineCart(cartItem)
//                                    }
//                                }
//                            }
//                            else{
//                                cartItem.sizeId = null
//                                cartItem.size = null
//                                addToOfflineCart(cartItem)
//                            }
//                        }
//                    }
//                }
//                else -> {
//                    addToOfflineCart(cartItem)
//                }
//            }
//        }
//        else
//        {
//            apiResponseLiveData.value = ApiResponse.success(getString(R.string.msg_zero_stock_quantity))
//        }
    }

    private fun addToOfflineCart(cartItem: CartItem) {

        when {
            cartsList.isNullOrEmpty() -> {
                cartsList?.add(cartItem)
                cartsList?.let { PrefMethods.saveUserCarts(it) }
                apiResponseLiveData.value = ApiResponse.success(Codes.ADDED_TO_CART)
            }
            else -> {
                var i = 0
                while (i < cartsList.size) {
                    when {
                        // This item is exist in cart >> update quantity
                        cartsList[i].id == productDetails.id -> {
                            cartsList[i].quantity = cartsList[i].quantity?.plus(1) // update quantity
                            cartsList[i].total = cartsList[i].total?.plus(productDetails.price!!) // update price
                            val sharedCart = PrefMethods.getUserCarts()
                            sharedCart?.clear()
                            sharedCart?.addAll(cartsList)
                            sharedCart?.let { PrefMethods.saveUserCarts(it) }
                            apiResponseLiveData.value = ApiResponse.success(Codes.ADDED_TO_CART)
                            return
                        }
                        i == cartsList.size - 1 -> {
                            // This item not exist in cart
                            cartsList.add(cartItem)
                            PrefMethods.saveUserCarts(cartsList)
                            apiResponseLiveData.value = ApiResponse.success(Codes.ADDED_TO_CART)
                            return
                        }
                        else -> i++
                    }
                }
            }
        }
       // apiResponseLiveData.value = ApiResponse.success(Codes.ADDED_TO_CART)
    }

    fun addToOnlineCart(addToCartRequest: AddToCartRequest) {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.ADD_TO_CART,
                addToCartRequest)
        }) { res ->
            obsIsProgress.set(false)
            val response: AddToCartResponse = Gson().fromJson(res, AddToCartResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.successMessage(response.message)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}
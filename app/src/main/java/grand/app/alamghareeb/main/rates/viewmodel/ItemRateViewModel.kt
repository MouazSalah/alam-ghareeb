package grand.app.alamghareeb.main.rates.viewmodel

import androidx.databinding.ObservableField
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.rates.response.RateItem

class ItemRateViewModel(var item: RateItem) : BaseViewModel()
{
    var obsRate = ObservableField<Float>()

    init {
        item.let {

            obsRate.set(item.rate!!.toFloat())

//            when {
//                item.createdAt != null -> {
//                    val format = SimpleDateFormat("d MMM yyyy  hh:mm aaa")
//                    try {
//                        date = format.parse(it.createdAt)
//                    } catch (e: ParseException) {
//                        e.printStackTrace()
//                    }
//
//                    val dayFormat = SimpleDateFormat("dd-MM-yyyy", Locale("en"))
//                    obsTime.set(dayFormat.format(date).toString());
//                }
//            }
        }
    }
}
package grand.app.alamghareeb.main.orders.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawMyOrderBinding
import grand.app.alamghareeb.main.orders.model.MyOrderItem
import grand.app.alamghareeb.main.orders.viewmodel.ItemMyOrderViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.resources.ResourceManager
import grand.app.alamghareeb.utils.resources.ResourceManager.getString
import timber.log.Timber
import kotlin.collections.ArrayList

class MyOrdersAdapter : RecyclerView.Adapter<MyOrdersAdapter.MyOrdersHolder>()
{
    var itemsList: ArrayList<MyOrderItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<MyOrderItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyOrdersHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawMyOrderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.raw_my_order, parent, false)
        return MyOrdersHolder(binding)
    }

    override fun onBindViewHolder(holder: MyOrdersHolder, position: Int) {
        val itemViewModel = ItemMyOrderViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        if (itemViewModel.item.payType == "branch") {
            holder.binding.tvTypeStatus.text = getString(R.string.label_watitng_to_deliver)
        } else {
            holder.binding.tvTypeStatus.text = getString(R.string.label_on_the_way)
        }

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }

        when (itemViewModel.item.historyList?.size) {
            1 -> {
                holder.binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivSecondStep.setImageResource(R.drawable.ic_order_unselected)
                holder.binding.ivThirdStep.setImageResource(R.drawable.ic_order_unselected)
                holder.binding.ivFourthStep.setImageResource(R.drawable.ic_order_unselected)

                holder.binding.ivOrderAccepted.background =
                    ResourceManager.getDrawable(R.drawable.unselected_status)
                holder.binding.ivOrderPrepared.background =
                    ResourceManager.getDrawable(R.drawable.unselected_status)
                holder.binding.ivOrderCompleted.background =
                    ResourceManager.getDrawable(R.drawable.unselected_status)
            }
            2 -> {
                holder.binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivSecondStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivThirdStep.setImageResource(R.drawable.ic_order_unselected)
                holder.binding.ivFourthStep.setImageResource(R.drawable.ic_order_unselected)
                holder.binding.ivOrderAccepted.background = ResourceManager.getDrawable(R.drawable.selected_status)
                holder.binding.ivOrderPrepared.background = ResourceManager.getDrawable(R.drawable.unselected_status)
                holder.binding.ivOrderCompleted.background = ResourceManager.getDrawable(R.drawable.unselected_status)
            }
            3 -> {
                holder.binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivSecondStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivThirdStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivFourthStep.setImageResource(R.drawable.ic_order_unselected)
                holder.binding.ivOrderAccepted.background = ResourceManager.getDrawable(R.drawable.selected_status)
                holder.binding.ivOrderPrepared.background = ResourceManager.getDrawable(R.drawable.selected_status)
                holder.binding.ivOrderCompleted.background = ResourceManager.getDrawable(R.drawable.unselected_status)
            }
            4 -> {
                holder.binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivSecondStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivThirdStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivFourthStep.setImageResource(R.drawable.ic_branch_checked)
                holder.binding.ivOrderAccepted.background = ResourceManager.getDrawable(R.drawable.selected_status)
                holder.binding.ivOrderPrepared.background = ResourceManager.getDrawable(R.drawable.selected_status)
                holder.binding.ivOrderCompleted.background = ResourceManager.getDrawable(R.drawable.selected_status)
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<MyOrderItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class MyOrdersHolder(val binding: RawMyOrderBinding) : RecyclerView.ViewHolder(binding.root)
}

package grand.app.alamghareeb.main.productdetails.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.ProductItem

data class ProductDetailsResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val productDetailsData: ProductDetailsData? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class ProductDetailsData(

	@field:SerializedName("images")
	val productImagesList: List<ProductSliderItem?>? = null,

	@field:SerializedName("is_favorite")
	var isFavorite: Boolean? = null,

	@field:SerializedName("is_new")
	val isNew: Int? = null,

	@field:SerializedName("validity_period")
	val validityPeriod: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("weight")
	val weight: String? = null,

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("related_products")
	val relatedProducts: List<ProductItem?>? = null,

	@field:SerializedName("price_before")
	val priceBefore: Double? = null,

	@field:SerializedName("rate_count")
	val rateCount: Int? = null,

	@field:SerializedName("rate")
	val rate: Float? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("sku")
	val serial: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("attributes")
	val colorsList: List<ProductColorItem?>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("brand")
	val brand: String? = null
)

data class ProductColorItem(

	@field:SerializedName("sizes")
	val sizesList: List<ProductSizeItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("value")
	val value: String? = null
)

data class ProductSliderItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("product_id")
	val productId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("sort_order")
	val sortOrder: Int? = null
)

data class ProductSizeItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

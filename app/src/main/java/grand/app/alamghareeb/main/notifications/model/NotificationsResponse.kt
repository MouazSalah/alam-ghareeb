package grand.app.alamghareeb.main.notifications.model

import com.google.gson.annotations.SerializedName

data class NotificationsResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val notificationsData: NotificationsData? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class NotificationsData(

	@field:SerializedName("data")
	val notificationsList: List<NotificationItem?>? = null,

	@field:SerializedName("meta")
	val metaData: MetaData? = null,

	@field:SerializedName("links")
	val linksData: LinksData? = null
)

data class MetaData(

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)

data class NotificationItem(

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("text")
	val text: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("order_id")
	val orderId: Int? = null
)

data class LinksData(

	@field:SerializedName("next")
	val next: Any? = null,

	@field:SerializedName("last")
	val last: String? = null,

	@field:SerializedName("prev")
	val prev: Any? = null,

	@field:SerializedName("first")
	val first: String? = null
)

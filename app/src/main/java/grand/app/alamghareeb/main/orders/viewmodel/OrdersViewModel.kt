package grand.app.alamghareeb.main.orders.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.orders.model.MyOrderItem
import grand.app.alamghareeb.main.orders.model.MyOrdersResponse
import grand.app.alamghareeb.main.orders.view.MyOrdersAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class OrdersViewModel @Inject constructor() : BaseViewModel()
{
    var adapter = MyOrdersAdapter()
    var obsShowFilter = ObservableField<Boolean>()

    fun getMyOrders()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.MY_ORDERS) }) { res ->
            val response : MyOrdersResponse = Gson().fromJson(res, MyOrdersResponse::class.java)
            when (response.code) {
                200 -> {
                    when (response.myOrdersData?.myOrdersList?.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                            obsShowFilter.set(false)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            obsShowFilter.set(true)
                            adapter.updateList(response.myOrdersData?.myOrdersList as ArrayList<MyOrderItem>)
                            apiResponseLiveData.value = ApiResponse.success(response)
                        }
                    }
                }
                else -> {
                    obsShowFilter.set(false)
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun filterOrders(orderBy : String)
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().filterOrders(URLS.FILTER_ORDERS , orderBy) }) { res ->
            val response : MyOrdersResponse = Gson().fromJson(res, MyOrdersResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    adapter.updateList(response.myOrdersData?.myOrdersList as ArrayList<MyOrderItem>)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }
    fun onSortClicked() {
        setValue(Codes.SORT_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getMyOrders()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
                obsShowFilter.set(false)
            }
        }
    }

    init {
        getUserStatus()
    }
}
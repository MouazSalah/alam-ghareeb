package grand.app.alamghareeb.main.adapter.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.home.model.SliderItem

class ItemSliderViewModel(var item: SliderItem) : BaseViewModel()
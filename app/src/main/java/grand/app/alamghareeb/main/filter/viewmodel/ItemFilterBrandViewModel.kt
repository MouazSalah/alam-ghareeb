package grand.app.alamghareeb.main.filter.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.filter.model.FilterBrandItem

class ItemFilterBrandViewModel(var item: FilterBrandItem) : BaseViewModel()

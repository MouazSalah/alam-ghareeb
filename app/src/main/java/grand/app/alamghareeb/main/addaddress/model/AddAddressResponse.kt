package grand.app.alamghareeb.main.addaddress.model

import com.google.gson.annotations.SerializedName

data class AddAddressResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Any? = null,

	@field:SerializedName("message")
	val message: String? = null
)

package grand.app.alamghareeb.main.addresses.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentAddressListBinding
import grand.app.alamghareeb.main.addaddress.view.AddAddressActivity
import grand.app.alamghareeb.main.addresses.model.CityItem
import grand.app.alamghareeb.main.addresses.model.ListAddressItem
import grand.app.alamghareeb.main.addresses.viewmodel.ListAddressViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class AddressesListFragment : BaseFragment()
{
    lateinit var binding : FragmentAddressListBinding
    @Inject
    lateinit var viewModel: ListAddressViewModel
    lateinit var addressItem : ListAddressItem

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_address_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is Int -> {
                            when (it.data) {
                                Codes.LOGIN_CLICKED -> {
                                    Const.isAskedToLogin = 1
                                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                }
                                Codes.ADD_ADDRESS_CLICKED -> {
                                    requireActivity().startActivityForResult(Intent(requireActivity(), AddAddressActivity::class.java), Codes.ADD_ADDRESS_REQUEST_CODE)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.adapter.addressLiveData) {
            when {
                it != null -> {
                    viewModel.setDefaultAddress(it)
                }
            }
        }

        observe(viewModel.adapter.removeLiveData) {
            if (it != null) {
                viewModel.deleteAddress(it)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /* When This page opened navigate user to select new location from The map first */
        when (requestCode) {
            /* When User back from Select Date and Time */
            Codes.ADD_ADDRESS_REQUEST_CODE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                       viewModel.getUserStatus()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /* Callback of confirming delete address item dialog */
        when {
            requestCode == Codes.DIALOG_CONFIRM_REQUEST && data != null -> {
                when {
                    data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                        when {
                            data.getIntExtra(Params.DIALOG_CLICK_ACTION,1) == 1 -> {
                                when {
                                    data.hasExtra(Params.DIALOG_ADDRESS_ITEM) -> {
//                                        val itemAddress = data.getParcelableExtra<ListAddressItem>(Params.DIALOG_ADDRESS_ITEM)
//                                        viewModel.deleteDeliveryAddress(itemAddress!!)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}
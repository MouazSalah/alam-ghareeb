package grand.app.alamghareeb.main.addresses.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.addresses.model.DeleteAddressResponse
import grand.app.alamghareeb.main.addresses.model.ListAddressItem
import grand.app.alamghareeb.main.addresses.model.ListAddressesResponse
import grand.app.alamghareeb.main.addresses.model.SetDefaultAddressRequest
import grand.app.alamghareeb.main.addresses.view.AddressAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class ListAddressViewModel @Inject constructor() : BaseViewModel()
{
    var adapter  = AddressAdapter()
    var obsShowAddBtn = ObservableField(false)

    private fun getAllAddresses()
    {
        obsShowAddBtn.set(false)
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.ALL_ADDRESSES)}) { res ->
            val response : ListAddressesResponse = Gson().fromJson(res, ListAddressesResponse::class.java)
            when (response.code) {
                200 -> {
                    obsShowAddBtn.set(true)
                    when (response.listAddressesData?.addressesList!!.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(response.listAddressesData.addressesList as ArrayList<ListAddressItem>)
                            notifyChange()
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onLoginClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getAllAddresses()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }

    fun onAddAddressClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.ADD_ADDRESS_CLICKED)
    }

    fun deleteAddress(item : ListAddressItem)
    {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestDeleteQuery(URLS.DELETE_ADDRESS, item.id!!)}) { res ->
            obsIsProgress.set(false)
            val response : DeleteAddressResponse = Gson().fromJson(res, DeleteAddressResponse::class.java)
            when (response.code) {
                200 -> {
                    adapter.removeItem(item)

                    when (adapter.itemsList.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                        }
                    }

                    apiResponseLiveData.value = ApiResponse.successMessage(response.message)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun setDefaultAddress(item : ListAddressItem)
    {
        val defaultRequest = SetDefaultAddressRequest(item.id!!)
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.SET_DEFAULT, defaultRequest)}) { res ->
            obsIsProgress.set(false)
            val response : DeleteAddressResponse = Gson().fromJson(res, DeleteAddressResponse::class.java)
            when (response.code) {
                200 -> {
                    item.isDefault = 1
                    val pos = adapter.itemsList.indexOf(item)
                    adapter.itemsList[pos].isDefault = 1
                    adapter.notifyItemChanged(pos)
                    getAllAddresses()
                    apiResponseLiveData.value = ApiResponse.successMessage(response.message)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}
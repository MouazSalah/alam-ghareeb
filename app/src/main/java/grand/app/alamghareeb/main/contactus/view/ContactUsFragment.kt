package grand.app.alamghareeb.main.contactus.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentContactUsBinding
import grand.app.alamghareeb.main.contactus.viewmodel.ContactUsViewModel
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class ContactUsFragment : BaseFragment()
{
    lateinit var binding: FragmentContactUsBinding
    @Inject lateinit var viewModel: ContactUsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            viewModel.getLinks()
        }

       observe(viewModel.adapter.itemLiveData)
       {
           when (it?.id) {
               0 -> {
                    Utils.openFacebook(requireActivity(), it.link)
               }
               1 -> {
                   Utils.openInstagram(requireActivity(), it.link)
               }
               2 -> {
                   Utils.openTwitter(requireActivity(), it.link)
               }
               3 -> {
                   Utils.openMail(requireActivity(), it.link)
               }
           }
       }
    }
}